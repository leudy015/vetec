var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const pagoSchema = new mongoose.Schema(
  {
    nombre: String,
    iban: String,
    profesionalID: String,
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

const Pago = mongoose.model("pago", pagoSchema);

module.exports = Pago;
