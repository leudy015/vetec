import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });

const mailjet = require("node-mailjet").connect(
  process.env.MJ_APIKEY_PUBLIC,
  process.env.MJ_APIKEY_PRIVATE
);

export function SaveEmail(email, name) {
  const request = mailjet.post("contact").request({
    Email: email,
    IsExcludedFromCampaigns: "false",
    Name: name,
  });
  request
    .then((result) => {
      const request = mailjet
        .post("contact")
        .id(result.body.Data[0].ID)
        .action("managecontactslists")
        .request({
          ContactsLists: [
            {
              ListID: 2442594,
              Action: "addnoforce",
            },
          ],
        });
      request
        .then((result) => {
          console.log("done");
        })
        .catch((err) => {
          console.log(err.statusCode);
        });
    })
    .catch((err) => {
      console.log(err.statusCode);
    });
}
