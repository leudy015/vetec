const express = require("express");
const stripeRouter = express.Router();
import { Types } from "mongoose";
const { ObjectId } = Types;
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
import { Usuarios, Consulta } from "../../src/database";
var cors = require("cors");
import { chargeToken } from "./stripe";
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);

const whitelist = [
  "https://vetec.es",
  "https://www.vetec.es",
  "https://profesionales.vetec.es",
  "https://dashboard.vetec.es",
];

const corsOptions = {
  origin: (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      //callback(new Error('Not allowed by CORS'));
      callback(null, true);
    }
  },
  optionsSuccessStatus: 200,
};

stripeRouter.get("/create-client", cors(corsOptions), async (req, res) => {
  const { userID, nameclient, email } = req.query;
  let response = await stripe.customers.create(
    {
      name: nameclient,
      email: email,
      description: "Clientes de Vetec App",
    },
    function (err, customer) {
      if (customer) {
        Usuarios.findOneAndUpdate(
          { _id: ObjectId(userID) },
          {
            $set: {
              StripeID: customer.id,
            },
          },
          (err, customers) => {
            if (err) {
              console.log(err);
            }
          }
        );
      }
    }
  );
  console.log(response);
});

stripeRouter.get("/card-create", async (req, res) => {
  const { customers, token } = req.query;
  stripe.customers.createSource(customers, { source: token }, function (
    err,
    card
  ) {
    if (err) {
      res.json(err.raw.code);
    } else {
      res.json(card);
    }
  });
});

stripeRouter.get("/create-suscription", cors(corsOptions), async (req, res) => {
  const { customers, UserID } = req.query;
  stripe.subscriptions.create(
    {
      customer: customers,
      items: [{ plan: "vetecplus" }],
    },
    function (err, subscription) {
      if (err) {
        console.log(err.raw.code);
      } else {
        console.log(subscription);
        if (subscription.status === "active") {
          Usuarios.findOneAndUpdate(
            { _id: ObjectId(UserID) },
            {
              $set: {
                isPlus: true,
                suscription: subscription,
                setVideoConsultas: 0,
              },
            },
            (err, user) => {
              if (err) {
                console.log(err);
              }
            }
          );
        }
        res.json(subscription);
      }
    }
  );
});

stripeRouter.get("/cancel-suscription", cors(corsOptions), async (req, res) => {
  const { suscription, UserID } = req.query;
  stripe.subscriptions.del(suscription, function (err, subscription) {
    if (err) {
      console.log(err);
      res.json(err);
    } else {
      res.json(subscription);
      if (subscription.status === "canceled") {
        Usuarios.findOneAndUpdate(
          { _id: ObjectId(UserID) },
          {
            $set: { isPlus: false, suscription: subscription },
          },
          (err, user) => {
            if (err) {
              console.log(err);
            }
          }
        );
      }
    }
  });
});

stripeRouter.get("/get-card", cors(corsOptions), async (req, res) => {
  const { customers } = req.query;
  const paymentMethods = await stripe.paymentMethods.list({
    customer: customers,
    type: "card",
  });
  res.json(paymentMethods);
});

stripeRouter.get("/delete-card", cors(corsOptions), async (req, res) => {
  const { cardID } = req.query;
  const done = await stripe.paymentMethods.detach(cardID);
  if (done) {
    res.send(done);
  } else {
    console.log("hay un error");
  }
});

stripeRouter.get(
  "/payment-existing-card",
  cors(corsOptions),
  async (req, res) => {
    const { cardID, customers, amount, ConsultaID } = req.query;
    try {
      const paymentIntent = await stripe.paymentIntents.create({
        amount: amount,
        currency: "eur",
        customer: customers,
        payment_method: cardID,
        off_session: true,
        confirm: true,
      });
      if (paymentIntent.status === "succeeded") {
        Consulta.findOneAndUpdate(
          { _id: ObjectId(ConsultaID) },
          {
            $set: {
              estado: "Nueva",
              progreso: "50",
              status: "active",
              stripePaymentIntent: paymentIntent,
            },
          },
          () => {
            res.json({ success: true, data: paymentIntent });
          }
        );
      } else {
        res.json({ success: false, data: paymentIntent });
      }
    } catch (err) {
      // Error code will be authentication_required if authentication is needed
      console.log("Error code is: ", err.code);
      res.json({ success: false, data: err.code });
      const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(
        err.raw.payment_intent.id
      );
      console.log("PI retrieved: ", paymentIntentRetrieved.id);
    }
  }
);

stripeRouter.get("/stripe/chargeToken", cors(corsOptions), (req, res) => {
  let data = req.query;
  if (data.stripeToken && data.amount != undefined) {
    chargeToken(data.amount, data.stripeToken)
      .then((response) => {
        if (response.status == "succeeded") {
          Consulta.findOneAndUpdate(
            { _id: ObjectId(data.orderId) },
            {
              $set: {
                estado: "Nueva",
                progreso: "50",
                status: "active",
                stripePaymentIntent: response,
              },
            },
            (err, order) => {
              res.status(200).send({ success: true, data: response });
            }
          );
        } else {
          res.status(401).send({ success: false, data: response });
        }
      })
      .catch((err) => {
        res.status(401).send({ success: false, error: err });
      });
  } else {
    res.status(403).send({ success: false, error: "Invalid token" });
  }
});

stripeRouter.post("/stripe/chargeToken-web", cors(corsOptions), (req, res) => {
  let data = req.body;
  if (data.stripeToken && data.amount != undefined) {
    chargeToken(data.amount, data.stripeToken)
      .then((response) => {
        if (response.status == "succeeded") {
          Consulta.findOneAndUpdate(
            { _id: ObjectId(data.orderId) },
            {
              $set: {
                estado: "Nueva",
                progreso: "50",
                status: "active",
                stripePaymentIntent: response,
              },
            },
            (err, order) => {
              res.send(response);
            }
          );
        } else {
          res.status(401).send({ success: false, data: response });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(401).send({ success: false, error: err });
      });
  } else {
    res.status(403).send({ success: false, error: "Invalid token" });
  }
});

module.exports = stripeRouter;
