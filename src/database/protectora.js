var mongoose = require("mongoose");
var bcrypt = require("bcryptjs");

mongoose.Promise = global.Promise;

const protectoraSchema = new mongoose.Schema(
  {
    name: String,
    email: String,
    logo: String,
    adrees: { type: mongoose.Schema.Types.Mixed },
    phone: String,
    password: String,
    descripcion: String,
    active: { type: Boolean, default: false },
    web: String,
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

// hashear los password antes de guardar
protectoraSchema.pre("save", function (next) {
  // Si el password no esta hasheado...
  if (!this.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(this.password, salt, (err, hash) => {
      if (err) return next(err);
      this.password = hash;
      next();
    });
  });
});

const Protectora = mongoose.model("protectora", protectoraSchema);

module.exports = Protectora;
