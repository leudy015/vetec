var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const diagnosticoSchema = new mongoose.Schema(
    {
      profesional: {
        nombre: String,
        apellidos: String,
        profesion: String,
        avatar: String,
      },
      diagnostico: String,
      mascota: { type: mongoose.Schema.Types.ObjectId, ref: "mascota" },
    },
    { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
  );
  
const Diagnostico = mongoose.model("diagnostico", diagnosticoSchema);

module.exports = Diagnostico;