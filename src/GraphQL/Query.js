import { Types } from "mongoose";
const { ObjectId } = Types;
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
import {
  Usuarios,
  Consulta,
  Mascota,
  Profesional,
  Cupones,
  Notification,
  Diagnostico,
  Deposito,
  Pago,
  Useradmin,
  Categories,
  UsuarioFavoritoPro,
  Conversation,
  ProfVisita,
  Adoptar,
  Protectora,
} from "./../database";

const actualizarProductoVisitar = async (profID) => {
  return new Promise(async (reject, resolve) => {
    if (!profID) {
      reject({
        success: false,
        message: "aun no hay un profesional",
      });
    }

    const date = new Date();
    const mes_id = date.getMonth();
    const ano = date.getFullYear();
    const condition = {
      profesional_id: profID,
      mes_id,
      ano,
    };

    const profVisitar = await ProfVisita.findOne(condition);

    if (profVisitar) {
      ProfVisita.findOneAndUpdate(
        { _id: profVisitar._id },
        { visitas: ++profVisitar.visitas },
        (error) => {
          if (error) {
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
            });
          } else {
            resolve({
              success: true,
              message: "Visita actualizada con éxito",
            });
          }
        }
      );
    } else {
      const data = { profesional_id: profID, visitas: 1, mes_id, ano };
      const nuevoProfVisitar = new ProfVisita(data);
      nuevoProfVisitar.save((error) => {
        if (error) {
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        } else {
          resolve({
            success: true,
            message: "Visita insertada con éxito",
          });
        }
      });
    }
  });
};

export const Query = {
  getmyPacientes: (root, { id, search }) => {
    return new Promise((resolve, reject) => {
      if (search) {
        Usuarios.find(
          { $text: { $search: `"\"${search} \""` }, MyVet: id, isPlus: true },
          { score: { $meta: "textScore" } },
          (error, favourite) => {
            if (error) {
              console.log("error ==>: ", error);

              reject({
                success: false,
                message: "Hay un problema con su solicitud",
                list: [],
              });
            } else {
              resolve({
                success: true,
                message: "Operación realizada con éxito",
                list: favourite,
              });
            }
          }
        );
      } else {
        Usuarios.find({ MyVet: id }, (error, favourite) => {
          if (error) {
            console.log("error ==>: ", error);

            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              list: [],
            });
          } else {
            resolve({
              success: true,
              message: "Operación realizada con éxito",
              list: favourite,
            });
          }
        });
      }
    });
  },

  getUserPlus: async (root, { id }) => {
    if (id) {
      return new Promise((resolve, reject) => {
        Usuarios.findOne({ _id: id }, (error, user) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              data: "",
            });
          else {
            resolve({
              success: true,
              message: "Operación realizada con éxito",
              data: user,
            });
          }
        });
      });
    } else {
      console.log("no hay un id de profesional");
    }
  },

  getAlluser: async (root) => {
    return new Promise((resolve, reject) => {
      Usuarios.find((error, user) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: "",
          });
        else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            data: user,
          });
        }
      });
    });
  },

  getProfVisitas: async (root, { profesionalId }) => {
    return new Promise((resolve, reject) => {
      ProfVisita.find(
        { profesional_id: profesionalId },
        (error, profVisitas) => {
          if (error) {
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          } else {
            resolve({
              success: true,
              message: "",
              list: profVisitas,
            });
          }
        }
      );
    });
  },

  getStatistics: (root, { userId }) => {
    return new Promise((resolve, reject) => {
      let matchQuery1 = {
        profesional: ObjectId(userId),
        estado: "Valorada",
      };

      let matchQuery2 = {
        profesional: ObjectId(userId),
        estado: "Rechazada",
      };

      Consulta.aggregate([
        { $match: matchQuery1 },
        {
          $group: {
            _id: {
              month: { $substr: ["$created_at", 5, 2] },
              estado: "Valorada",
            },
            stripePaymentIntent: { $first: "$stripePaymentIntent" },
            pagoPaypal: { $first: "$pagoPaypal" },
            paypalAmount: { $sum: "$pagoPaypal.montoPagado" },
            stripeAmount: { $sum: "$stripePaymentIntent.amount" },
            finishedCount: { $sum: 1 },
            created_at: { $first: "$created_at" },
          },
        },
        {
          $project: {
            your_year_variable: { $year: "$created_at" },
            paypalAmount: 1,
            finishedCount: 1,
            stripeAmountDivide: { $divide: ["$stripeAmount", 100] },
          },
        },
        { $match: { your_year_variable: 2020 } },
      ])
        .then((res) => {
          let ordenes = {
            name: "Consultas",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };
          let ganacias = {
            name: "Ganacias",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };
          let devoluciones = {
            name: "Rechazos",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };

          for (let i = 0; i < res.length; i++) {
            let month = res[i]._id.month;
            if (month == "01") {
              ganacias["Ene"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["Ene"] = res[i].finishedCount;
            } else if (month == "02") {
              ganacias["Feb"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["Feb"] = res[i].finishedCount;
            } else if (month == "03") {
              ganacias["Mar"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["Mar"] = res[i].finishedCount;
            } else if (month == "04") {
              ganacias["Abr"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["Abr"] = res[i].finishedCount;
            } else if (month == "05") {
              ganacias["May"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["May"] = res[i].finishedCount;
            } else if (month == "06") {
              ganacias["Jun"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["Jun"] = res[i].finishedCount;
            } else if (month == "07") {
              ganacias["Jul"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["Jul"] = res[i].finishedCount;
            } else if (month == "08") {
              ganacias["Aug"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["Aug"] = res[i].finishedCount;
            } else if (month == "09") {
              ganacias["Sep"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["Sep"] = res[i].finishedCount;
            } else if (month == "10") {
              ganacias["Oct"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["Oct"] = res[i].finishedCount;
            } else if (month == "11") {
              ganacias["Nov"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["Nov"] = res[i].finishedCount;
            } else if (month == "12") {
              ganacias["Dic"] =
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                (isNaN(res[i].stripeAmountDivide)
                  ? 0
                  : res[i].stripeAmountDivide);
              ordenes["Dic"] = res[i].finishedCount;
            }
          }

          Consulta.aggregate([
            { $match: matchQuery2 },
            {
              $group: {
                _id: {
                  month: { $substr: ["$created_at", 5, 2] },
                  estado: "Rechazada",
                },

                returnedCount: { $sum: 1 },
                created_at: { $first: "$created_at" },
              },
            },
            {
              $project: {
                your_year_variable: { $year: "$created_at" },
                returnedCount: 1,
              },
            },
            { $match: { your_year_variable: 2020 } },
          ]).then((res1) => {
            for (let i = 0; i < res1.length; i++) {
              let month = res1[i]._id.month;
              if (month == "01") {
                devoluciones["Ene"] = res1[i].returnedCount;
              } else if (month == "02") {
                devoluciones["Feb"] = res1[i].returnedCount;
              } else if (month == "03") {
                devoluciones["Mar"] = res1[i].returnedCount;
              } else if (month == "04") {
                devoluciones["Abr"] = res1[i].returnedCount;
              } else if (month == "05") {
                devoluciones["May"] = res1[i].returnedCount;
              } else if (month == "06") {
                devoluciones["Jun"] = res1[i].returnedCount;
              } else if (month == "07") {
                devoluciones["Jul"] = res1[i].returnedCount;
              } else if (month == "08") {
                devoluciones["Aug"] = res1[i].returnedCount;
              } else if (month == "09") {
                devoluciones["Sep"] = res1[i].returnedCount;
              } else if (month == "10") {
                devoluciones["Oct"] = res1[i].returnedCount;
              } else if (month == "11") {
                devoluciones["Nov"] = res1[i].returnedCount;
              } else if (month == "12") {
                devoluciones["Dic"] = res1[i].returnedCount;
              }
            }
            resolve({
              success: true,
              message: "",
              data: [ordenes, ganacias, devoluciones],
            });
            //console.log(ordenes, ganacias, devoluciones);
          });
        })
        .catch((err) => {
          console.log(err);
        });
    });
  },

  getConversationclient: (root, { userId }) => {
    if (userId) {
      return new Promise((resolve, reject) => {
        Conversation.find({ user: userId, available: true })
          .sort({ $natural: -1 })
          .exec((error, conversation) => {
            if (error) {
              console.log(error);
              reject({
                success: false,
                message: "Hay un problema con tu solicitud",
                conversation: [],
              });
            } else {
              resolve({
                success: true,
                message: "success",
                conversation: conversation,
              });
            }
          });
      });
    } else {
      return null;
    }
  },

  getConversationProf: (root, { ProfId }) => {
    if (ProfId) {
      return new Promise((resolve, reject) => {
        Conversation.find({ prof: ProfId })
          .sort({ $natural: -1 })
          .exec((error, conversation) => {
            if (error) {
              console.log(error);
              reject({
                success: false,
                message: "Hay un problema con tu solicitud",
                conversation: [],
              });
            } else {
              resolve({
                success: true,
                message: "success",
                conversation: conversation,
              });
            }
          });
      });
    } else {
      return null;
    }
  },

  getMessageNoReadClient: (root, { conversationID, profId }) => {
    if (profId) {
      return new Promise((resolve, reject) => {
        Conversation.aggregate([
          {
            $match: {
              _id: ObjectId(conversationID),
            },
          },
          {
            $unwind: "$messagechat",
          },
          {
            $match: {
              "messagechat.read": false,
              "messagechat.user._id": profId,
            },
          },
          {
            $group: {
              _id: null,
              count: { $sum: 1 },
            },
          },
        ]).exec((error, conversation) => {
          if (error) {
            console.log(error);
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              conversation: [],
            });
          } else {
            resolve({
              success: true,
              message: "success",
              conversation: conversation,
            });
          }
        });
      });
    } else {
      return null;
    }
  },

  getMessageNoReadProf: (root, { conversationID, clientId }) => {
    if (clientId) {
      return new Promise((resolve, reject) => {
        Conversation.aggregate([
          {
            $match: {
              _id: ObjectId(conversationID),
            },
          },
          {
            $unwind: "$messagechat",
          },
          {
            $match: {
              "messagechat.read": false,
              "messagechat.user._id": clientId,
            },
          },
          {
            $group: {
              _id: null,
              count: { $sum: 1 },
            },
          },
        ]).exec((error, conversation) => {
          if (error) {
            console.log(error);
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              conversation: [],
            });
          } else {
            resolve({
              success: true,
              message: "success",
              conversation: conversation,
            });
          }
        });
      });
    } else {
      return null;
    }
  },

  getUsuario: (root, { id }) => {
    return new Promise((resolve, reject) => {
      Usuarios.findById(id, (error, user) => {
        if (error) reject(error);
        else resolve(user);
      });
    });
  },

  getProfesionalall: () => {
    return Profesional.find({ isAvailable: true }).sort({ $natural: -1 });
  },

  getProfesionaforSuscribcion: () => {
    return Profesional.find({ isPlusVet: true });
  },

  getProfesionalone: (root, { id, updateProfVisit }) => {
    if (updateProfVisit) actualizarProductoVisitar(id);
    return new Promise((resolve, reject) => {
      Profesional.findById(
        { _id: id, isAvailable: true },
        (error, profeisonal) => {
          if (error)
            reject({
              success: false,
              message: "Hubo u problema con su solicitud",
              data: null,
            });
          else {
            resolve({
              success: true,
              message: "Solicitud obtenida con éxito",
              data: profeisonal,
            });
          }
        }
      );
    });
  },

  getAllCupon: () => {
    return Cupones.find();
  },

  getCategory: async (root, { id }) => {
    return new Promise((resolve, reject) => {
      Categories.findById(id, (error, category) => {
        if (error)
          reject({
            success: false,
            message: "Hay un problema con su sulicitud",
            data: null,
          });
        else
          resolve({
            success: true,
            message: "Profecionales obtenido con éxito",
            data: category,
          });
      }).sort({ $natural: -1 });
    });
  },

  getUsuarioFavoritoPro: (root, { id }) => {
    return new Promise((resolve, reject) => {
      UsuarioFavoritoPro.find({ usuarioId: id }, (error, favourite) => {
        if (error) {
          console.log("error ==>: ", error);

          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: favourite,
          });
        }
      });
    });
  },

  getUsuarioFavoritoall: (root, { id }) => {
    return new Promise((resolve, reject) => {
      UsuarioFavoritoPro.find({ profesionalId: id }, (error, favourite) => {
        if (error) {
          console.log("error ==>: ", error);

          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: favourite,
          });
        }
      });
    });
  },

  getProfesionalCity: async (root, { city }) => {
    return new Promise((resolve, reject) => {
      Profesional.find(
        { ciudad: city, isAvailable: true },
        (error, profeisonal) => {
          if (error)
            reject({
              success: false,
              message: "Hubo u problema con su solicitud",
              data: null,
            });
          else {
            resolve({
              success: true,
              message: "Solicitud obtenida con éxito",
              data: profeisonal,
            });
          }
        }
      ).sort({ $natural: -1 });
    });
  },

  getProfesionalSearch: async (root, { search }) => {
    return new Promise((resolve, reject) => {
      Profesional.find(
        { $text: { $search: `"\"${search} \""` }, isAvailable: true },
        { score: { $meta: "textScore" } },
        (error, profesional) => {
          if (error)
            reject(
              {
                success: false,
                message: "Hubo u problema con su solicitud",
                data: null,
              },
              console.log(error)
            );
          else {
            resolve({ success: true, message: "", data: profesional });
          }
        }
      ).sort({ score: { $meta: "textScore" } });
    });
  },

  getProfesionalSearchadmin: async (root, { search }) => {
    return new Promise((resolve, reject) => {
      if (search) {
        Profesional.find(
          { $text: { $search: `"\"${search} \""` } },
          { score: { $meta: "textScore" } },
          (error, profesional) => {
            if (error)
              reject(
                {
                  success: false,
                  message: "Hubo u problema con su solicitud",
                  data: null,
                },
                console.log(error)
              );
            else {
              resolve({
                success: true,
                message: "",
                data: profesional,
              });
            }
          }
        ).sort({ score: { $meta: "textScore" } });
      } else {
        Profesional.find((error, profesional) => {
          if (error) {
            return reject(
              {
                success: false,
                message: "Hubo u problema con su solicitud",
                data: null,
              },
              console.log(error)
            );
          } else {
            return resolve({
              success: true,
              message: "",
              data: profesional,
            });
          }
        });
      }
    });
  },

  getTeam: async (root, { id }) => {
    return new Promise((resolve, reject) => {
      Useradmin.findOne({ _id: id }, (error, team) => {
        if (error) {
          return reject(
            {
              success: false,
              message: "Hubo u problema con su solicitud",
              data: null,
            },
            console.log(error)
          );
        } else {
          return resolve({ success: true, message: "", data: team });
        }
      });
    });
  },

  getTeamSearch: async (root) => {
    return new Promise((resolve, reject) => {
      Useradmin.find((error, team) => {
        if (error) {
          return reject(
            {
              success: false,
              message: "Hubo u problema con su solicitud",
              data: null,
            },
            console.log(error)
          );
        } else {
          return resolve({
            success: true,
            message: "Operación realizada con éxito",
            data: team,
          });
        }
      });
    });
  },

  getClientesSearchadmin: async (root, { search }) => {
    return new Promise((resolve, reject) => {
      if (search) {
        Usuarios.find(
          { $text: { $search: `"\"${search} \""` } },
          { score: { $meta: "textScore" } },
          (error, usuario) => {
            if (error)
              reject(
                {
                  success: false,
                  message: "Hubo u problema con su solicitud",
                  data: null,
                },
                console.log(error)
              );
            else {
              resolve({ success: true, message: "", data: usuario });
            }
          }
        ).sort({ score: { $meta: "textScore" } });
      } else {
        Usuarios.find((error, usuario) => {
          if (error) {
            return reject(
              {
                success: false,
                message: "Hubo u problema con su solicitud",
                data: null,
              },
              console.log(error)
            );
          } else {
            return resolve({
              success: true,
              message: "",
              data: usuario,
            });
          }
        });
      }
    });
  },

  getMascotaadmin: (root, { id }) => {
    return new Promise((resolve, reject) => {
      if (id) {
        Mascota.find({ _id: id }, (error, mascota) => {
          if (error) reject(error);
          else
            resolve({
              success: true,
              message: "Solicitud procesada con éxito",
              list: mascota,
            });
        });
      } else {
        Mascota.find((error, mascota) => {
          if (error) reject(error);
          else
            resolve({
              success: true,
              message: "Solicitud procesada con éxito",
              list: mascota,
            });
        });
      }
    });
  },

  getMascotaprotectora: (root, { id }) => {
    return new Promise((resolve, reject) => {
      Mascota.find({ protectoraID: id }, (error, mascota) => {
        if (error) reject(error);
        else
          resolve({
            success: true,
            message: "Solicitud procesada con éxito",
            list: mascota,
          });
      }).sort({ $natural: -1 });
    });
  },

  getMascotaforAdopcionClients: (root, { especie, ciudad, genero }) => {
    let condition = { protectora: true };
    if (especie) condition.especie = especie;

    if (genero) condition.genero = genero;

    if (ciudad) condition.ciudad = ciudad;

    return new Promise((resolve, reject) => {
      Mascota.find(condition, (error, mascota) => {
        if (error) reject(error);
        else
          resolve({
            success: true,
            message: "Solicitud procesada con éxito",
            list: mascota,
          });
      }).sort({ $natural: -1 });
    });
  },

  getProfesional: (root, { id }) => {
    return new Promise((resolve, reject) => {
      Profesional.findById(id, (error, profesional) => {
        if (error) reject(error);
        else resolve(profesional);
      });
    });
  },

  getAdmin: (root, { id }) => {
    return new Promise((resolve, reject) => {
      Useradmin.findById(id, (error, admin) => {
        if (error) reject(error);
        else resolve(admin);
      });
    });
  },

  getMascota: (root, { usuarios }) => {
    return new Promise((resolve, reject) => {
      Mascota.find({ usuario: usuarios }, (error, mascota) => {
        if (error) reject(error);
        else
          resolve({
            success: true,
            message: "Solicitud procesada con éxito",
            list: mascota,
          });
      });
    });
  },

  getMascotaForAdopcion: (root, { protectoraID }) => {
    return new Promise((resolve, reject) => {
      Mascota.find(
        { protectoraID: protectoraID, protectora: true },
        (error, mascota) => {
          if (error) reject(error);
          else
            resolve({
              success: true,
              message: "Solicitud procesada con éxito",
              list: mascota,
            });
        }
      );
    });
  },

  getMascotaAdopciones: (root) => {
    return new Promise((resolve, reject) => {
      Mascota.find({ protectora: true }, (error, mascota) => {
        if (error) reject(error);
        else
          resolve({
            success: true,
            message: "Solicitud procesada con éxito",
            list: mascota,
          });
      });
    });
  },

  getDiagnostico: (root, { id }) => {
    return new Promise((resolve, reject) => {
      Diagnostico.find({ mascota: id }, (error, diagnostico) => {
        if (error) reject(error);
        else
          resolve({
            success: true,
            message: "Solicitud procesada con éxito",
            list: diagnostico,
          });
      }).sort({ $natural: -1 });
    });
  },

  obtenerUsuario: (root, args, { usuarioActual }) => {
    if (!usuarioActual) {
      return null;
    }
    const usuario = Usuarios.find(usuarioActual);

    return usuario;
  },

  obtenerAdmin: (root, args, { usuarioActual }) => {
    if (!usuarioActual) {
      return null;
    }
    const admin = Useradmin.find(usuarioActual);

    return admin;
  },

  obtenerProfesinal: (root, args, { usuarioActual }) => {
    if (!usuarioActual) {
      return null;
    }
    const profesional = Profesional.find(usuarioActual);
    return profesional;
  },

  getConsultaByProfessional: async (
    root,
    { profesional, dateRange },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    const usuario = await Profesional.findOne({
      usuario: usuarioActual.profeisonal,
    });
    if (!usuario) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    return new Promise((resolve, reject) => {
      let condition = {
        profesional: profesional,
        estado: { $nin: ["Pendiente de pago", "Valorada"] },
      };
      //let condition = { profesional: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      Consulta.find(
        condition,
        [],
        { sort: { created_at: -1 } },
        (error, consulta) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({ success: true, message: "", list: consulta });
          }
        }
      );
    });
  },

  getConsultaadmin: async (root, { dateRange, search }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }
    return new Promise((resolve, reject) => {
      if (!search) {
        let condition = { cantidad: 1 };
        if (dateRange && dateRange.fromDate && dateRange.toDate) {
          condition.created_at = {
            $gte: dateRange.fromDate,
            $lte: dateRange.toDate,
          };
        }
        Consulta.find(
          condition,
          [],
          { sort: { created_at: -1 } },
          (error, consulta) => {
            if (error)
              reject({
                success: false,
                message: "Hubo un problema con su solicitud",
                list: [],
              });
            else {
              resolve({ success: true, message: "", list: consulta });
            }
          }
        );
      } else {
        let condition = { _id: search };
        if (dateRange && dateRange.fromDate && dateRange.toDate) {
          condition.created_at = {
            $gte: dateRange.fromDate,
            $lte: dateRange.toDate,
          };
        }
        Consulta.find(condition, (error, consulta) => {
          if (error)
            reject(
              {
                success: false,
                message: "Hubo u problema con su solicitud",
                data: null,
              },
              console.log(error)
            );
          else {
            resolve({ success: true, message: "", list: consulta });
          }
        });
      }
    });
  },

  getCategories: () => {
    return Categories.find({});
  },

  getCupon: async (root, { clave }, { usuarioActual }) => {
    try {
      if (!usuarioActual) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }
      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario,
      });
      if (!usuario) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }
      return new Promise((resolve, reject) => {
        Cupones.findOne({ clave }, (error, cupon) => {
          if (error) {
            console.log("resolvers -> query -> getCupon -> error1 ", error);
            return reject(error);
          } else {
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      console.log("resolvers -> query -> getCupon -> error2 ", error);
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getCuponAll: async (root) => {
    try {
      return new Promise((resolve, reject) => {
        Cupones.findOne((error, cupon) => {
          if (error) {
            console.log("resolvers -> query -> getCupon -> error1 ", error);
            return reject(error);
          } else {
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      console.log("resolvers -> query -> getCupon -> error2 ", error);
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getFullConsulta: async (root, { id }, { usuarioActual }) => {
    try {
      if (!usuarioActual) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }

      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario,
      });

      if (!usuario) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }

      let consulta = await Consulta.findById({ _id: id })
        .populate("cupon")
        .populate("usuario")
        .populate("profesionals");
      return new Promise((resolve, reject) => {
        if (consulta) {
          resolve(consulta);
        } else {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            data: null,
          });
        }
      });
    } catch (error) {
      console.log("resolvers -> query -> getOrden -> error2 ", error);
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getConsulta: async (root, { id }, { usuarioActual }) => {
    try {
      if (!usuarioActual) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }

      const usuario = await Usuarios.findOne({
        usuario: usuarioActual.usuario,
      });

      if (!usuario) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }

      return Consulta.findOne({ _id: ObjectId(id) }).populate("descuento");
    } catch (error) {
      console.log("resolvers -> query -> getOrden -> error2 ", error);
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getDeposito: async (root, { id }) => {
    return new Promise((resolve, reject) => {
      Deposito.find({ profesionalID: id })
        .sort({ $natural: -1 })
        .exec((error, deposito) => {
          if (error)
            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              list: [],
            });
          else
            resolve({
              success: true,
              message: "solicitud procesada con éxito",
              list: deposito,
            });
        });
    });
  },

  getPago: async (root, { id }) => {
    return new Promise((resolve, reject) => {
      Pago.findOne({ profesionalID: id }, (error, pagos) => {
        if (error)
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            data: null,
          });
        else
          resolve({
            success: true,
            message: "Operacion realizada con éxito",
            data: pagos,
          });
      });
    });
  },

  getConsultaByPayment: async (root, { id }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }
    return new Promise((resolve, reject) => {
      let condition = { _id: id };
      Consulta.findById(condition, (error, consulta) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
            list: [],
          });
        else {
          resolve({
            success: true,
            message: "Consulta extraida con éxito",
            list: consulta,
          });
        }
      }).sort({ $natural: -1 });
    });
  },

  getConsultaByUsuario: async (
    root,
    { usuarios, dateRange },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    const usuario = await Usuarios.findOne({
      usuario: usuarioActual.usuario,
    });
    if (!usuario) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    return new Promise((resolve, reject) => {
      let condition = { usuario: usuarios, estado: { $ne: "Valorada" } };
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      Consulta.find(condition, (error, consulta) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
            list: [],
          });
        else {
          resolve({
            success: true,
            message: "Consulta extraida con éxito",
            list: consulta,
          });
        }
      }).sort({ $natural: -1 });
    });
  },

  getProfessionalRating: async (root, { id }) => {
    return new Promise((resolve, reject) => {
      Consulta.find(
        { profesional: id, estado: "Valorada" },
        [],
        { sort: { updated_at: -1 } },
        (error, consulta) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else
            resolve({
              success: true,
              message: "Opiniones obtenida con éxito",
              list: consulta,
            });
        }
      );
    });
  },

  getNotifications: (root, { Id }) => {
    if (Id) {
      return new Promise((resolve, reject) => {
        Notification.find({ user: Id, read: false })
          .populate("usuario")
          .populate("profesional")
          .populate("consulta")
          .sort({ $natural: -1 })
          .exec((error, notification) => {
            if (error) {
              console.log(error);
              reject({
                success: false,
                message: "Hubo un problema con su solicitud",
                notifications: [],
              });
            } else {
              resolve({
                success: true,
                message: "success",
                notifications: notification,
              });
            }
          });
      });
    } else {
      return null;
    }
  },

  getProtectoras: (root, { id }) => {
    return new Promise((resolve, reject) => {
      Protectora.findById(id, (error, protectora) => {
        if (error) reject(error);
        else resolve(protectora);
      });
    });
  },

  getAdopcionClient: (root, { usuario }) => {
    return new Promise((resolve, reject) => {
      Adoptar.find({ usuario: usuario }, (error, adopcion) => {
        if (error) reject(error);
        else
          resolve({
            success: true,
            message: "Solicitud procesada con éxito",
            data: adopcion,
          });
      }).sort({ $natural: -1 });
    });
  },

  getAdopcionProtectora: (root, { protectora, estado }) => {
    let condition = { protectora: protectora };
    if (estado) condition.estado = estado;
    return new Promise((resolve, reject) => {
      Adoptar.find(condition, (error, adopcion) => {
        if (error) reject(error);
        else
          resolve({
            success: true,
            message: "Solicitud procesada con éxito",
            data: adopcion,
          });
      }).sort({ $natural: -1 });
    });
  },
};
