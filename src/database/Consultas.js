var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const consultaSchema = new mongoose.Schema(
  {
    endDate: String,
    time: String,
    cupon: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "cupones",
    },
    nota: String,
    coment: String,
    descripcionproblem: String,
    rate: Number,
    aceptaTerminos: { type: Boolean, default: false },
    cantidad: { type: Number, default: 1 },
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: "usuarios" },
    mascota: { type: mongoose.Schema.Types.ObjectId, ref: "mascota" },
    profesional: { type: mongoose.Schema.Types.ObjectId, ref: "profesionals" },
    pagoPaypal: { type: mongoose.Schema.Types.Mixed },
    stripePaymentIntent: { type: mongoose.Schema.Types.Mixed },
    estado: {
      type: String,
      enum: [
        "Pendiente de pago",
        "Nueva",
        "Aceptado",
        "Completada",
        "Valorada",
        "Devuelto",
        "Rechazada",
      ],
      default: "Pendiente de pago",
    },
    progreso: {
      type: String,
      enum: ["0", "25", "50", "75", "100"],
      default: "25",
    },
    status: {
      type: String,
      enum: ["success", "exception", "normal", "active"],
      default: "active",
    },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

consultaSchema.virtual("descuento", {
  ref: "cupones",
  localField: "cupon",
  foreignField: "_id",
  justOne: true,
});

const Consulta = mongoose.model("consultas", consultaSchema);

module.exports = Consulta;
