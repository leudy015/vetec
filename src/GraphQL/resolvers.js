import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
import { Query } from "./Query";
import { Mutation } from "./Mutations";
import {
  Usuarios,
  Consulta,
  Mascota,
  Profesional,
  Cupones,
  UsuarioFavoritoPro,
  ProfVisita,
  Protectora,
} from "../database";

export const resolvers = {
  Query,
  Mutation,

  Adoptar: {
    macotas(parent) {
      return new Promise((resolve, reject) => {
        Mascota.findOne({ _id: parent.mascota }, (error, res) => {
          if (error) reject(error);
          else {
            resolve(res);
          }
        });
      });
    },
  },

  Mascota: {
    centro(parent) {
      return new Promise((resolve, reject) => {
        Protectora.findOne({ _id: parent.protectoraID }, (error, res) => {
          if (error) reject(error);
          else {
            resolve(res);
          }
        });
      });
    },
  },

  ConversationByClient: {
    usuario(parent) {
      return new Promise((resolve, reject) => {
        Usuarios.findOne({ _id: parent.user }, (error, res) => {
          if (error) reject(error);
          else {
            resolve(res);
          }
        });
      });
    },
    profesional(parent) {
      return new Promise((resolve, reject) => {
        Profesional.findOne({ _id: parent.prof }, (error, res) => {
          if (error) reject(error);
          else {
            resolve(res);
          }
        });
      });
    },
  },

  UsuarioPlus: {
    usuario(parent) {
      return new Promise((resolve, reject) => {
        Usuarios.findOne({ _id: parent._id }, (error, res) => {
          if (error) reject(error);
          else {
            resolve(res);
          }
        });
      });
    },
    profesional(parent) {
      return new Promise((resolve, reject) => {
        Profesional.findOne({ _id: parent.MyVet }, (error, res) => {
          if (error) reject(error);
          else {
            resolve(res);
          }
        });
      });
    },
  },

  Consultas: {
    profesional(parent) {
      return new Promise((resolve, reject) => {
        Profesional.findOne({ _id: parent.profesional }, (error, res) => {
          if (error) reject(error);
          else {
            resolve(res);
          }
        });
      });
    },

    descuento(parent) {
      return new Promise((resolve, reject) => {
        Cupones.findOne({ _id: parent.cupon }, (error, response) => {
          if (error) reject(error);
          else resolve(response);
        });
      });
    },

    usuario(parent) {
      return new Promise((resolve, reject) => {
        Usuarios.findOne({ _id: parent.usuario }, (error, usuario) => {
          if (error) reject(error);
          else resolve(usuario);
        });
      });
    },
    mascota(parent) {
      return new Promise((resolve, reject) => {
        Mascota.findOne({ _id: parent.mascota }, (error, mascota) => {
          if (error) reject(error);
          else resolve(mascota);
        });
      });
    },

    ConsultaRating(parent) {
      return new Promise((resolve, reject) => {
        Consulta.find(
          { profesional: parent.profesional, estado: "Valorada" },
          [],
          { sort: { updated_at: -1 } },
          (error, consulta) => {
            if (error) reject(error);
            else resolve(consulta);
          }
        );
      });
    },
  },

  ProfessionalRating: {
    customer(parent) {
      return new Promise((resolve, reject) => {
        Usuarios.findOne({ _id: parent.usuario }, (error, customer) => {
          if (error) reject(error);
          else resolve(customer);
        });
      });
    },
  },

  Profesional: {
    visitas(parent) {
      return new Promise((resolve, reject) => {
        ProfVisita.find(
          { profesional_id: parent._id },
          (error, profVisitas) => {
            if (error) reject(error);
            else {
              if (profVisitas.length === 0) resolve(0);
              else {
                let count = 0;
                profVisitas.forEach((element) => {
                  count += element.visitas;
                });
                resolve(count);
              }
            }
          }
        );
      });
    },

    professionalRatinglist(parent) {
      return new Promise((resolve, reject) => {
        Consulta.find(
          { profesional: parent._id, estado: "Valorada" },
          [],
          { sort: { updated_at: -1 } },
          (error, consulta) => {
            if (error) reject(error);
            else resolve(consulta);
          }
        );
      });
    },

    consultas(parent) {
      return new Promise((resolve, reject) => {
        Consulta.find({ profesional: parent._id }, (error, consultas) => {
          if (error) reject(error);
          else resolve(consultas);
        });
      });
    },

    anadidoFavorito(parent, args, { usuarioActual }) {
      return new Promise((resolve, reject) => {
        if (!usuarioActual) resolve(false);
        Usuarios.findOne(
          { usuario: usuarioActual.usuario },
          (error, usuario) => {
            if (error) reject(error);
            else {
              if (!usuario) resolve(false);
              else {
                UsuarioFavoritoPro.findOne(
                  { usuarioId: usuario._id, profesionalId: parent._id },
                  (error, favorito) => {
                    if (error) reject(error);
                    else {
                      if (!favorito) resolve(false);
                      else resolve(true);
                    }
                  }
                );
              }
            }
          }
        );
      });
    },
  },

  Category: {
    profesionales(parent, args, context, { variableValues: { price, city } }) {
      let condition = { category_id: parent._doc._id };
      if (price) condition.number = { $lte: price };
      if (city) condition.city = city;
      return new Promise((resolve, reject) => {
        Profesional.find(condition, (error, profeisonal) => {
          if (error) reject(error);
          else resolve(profeisonal);
        });
      });
    },
  },

  UsuarioFavoritoPro: {
    profesional(parent) {
      return new Promise((resolve, reject) => {
        Profesional.findById(parent.profesionalId, (error, profesional) => {
          if (error) {
            console.log("erro in chiild: ", error);
            reject(error);
          } else {
            resolve(profesional);
          }
        });
      });
    },
  },
};
