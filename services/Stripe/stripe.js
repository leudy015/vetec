import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });

const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);

export const chargeToken = async (amount, stripeToken) => {
  let response = await stripe.charges.create({
    amount: amount,
    currency: "EUR",
    source: stripeToken,
    capture: true,
  });
  return response;
};
