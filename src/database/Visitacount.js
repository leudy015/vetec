var mongoose = require("mongoose");

mongoose.Promise = global.Promise;


const profVisitasSchema = new mongoose.Schema(
    {
      profesional_id: String,
      mes_id: Number,
      ano: Number,
      visitas: Number,
    },
    { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
  );
  
const ProfVisita = mongoose.model("profVisita", profVisitasSchema);

module.exports = ProfVisita;