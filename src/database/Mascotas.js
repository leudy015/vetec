var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const mascotaSchema = new mongoose.Schema(
  {
    name: String,
    age: String,
    avatar: String,
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: "usuarios" },
    especie: String,
    raza: String,
    genero: String,
    peso: String,
    microchip: String,
    vacunas: String,
    alergias: String,
    protectoraID: String,
    ciudad: String,
    descripcion: String,
    protectora: { type: mongoose.Schema.Types.Boolean, default: false },
    castrado: { type: mongoose.Schema.Types.Boolean, default: false },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

const Mascota = mongoose.model("mascota", mascotaSchema);

module.exports = Mascota;
