var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const categoriesSchema = new mongoose.Schema(
    {
      title: String,
      image: String,
      description: String,
      font: String,
      img: String,
    },
    { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
  );
  
const Categories = mongoose.model("categories", categoriesSchema);

module.exports = Categories;