var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const solocitudadopcionSchema = new mongoose.Schema(
  {
    name: String,
    lastName: String,
    email: String,
    city: String,
    phone: String,
    experiencia: String,
    otra: String,
    fecha: Date,
    nino: Boolean,
    mascota: String,
    usuario: String,
    protectora: String,
    estado: String,
    razon: String,
    UserID: String,
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

const Adoptar = mongoose.model("solicitudadotar", solocitudadopcionSchema);

module.exports = Adoptar;
