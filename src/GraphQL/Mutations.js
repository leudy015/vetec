import { Types } from "mongoose";
var bcrypt = require("bcryptjs");
const { ObjectId } = Types;
import crypto from "crypto";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import fs, { read } from "fs";
dotenv.config({ path: "variables.env" });
import {
  Usuarios,
  Consulta,
  Mascota,
  Profesional,
  Cupones,
  Notification,
  Diagnostico,
  Deposito,
  Pago,
  Useradmin,
  Categories,
  UsuarioFavoritoPro,
  Conversation,
  ProfVisita,
  Adoptar,
  Protectora,
} from "./../database";
const nodemailer = require("nodemailer");
import confirmEmail from "../EmailSistems/ConfirmEmail";
import { SaveEmail } from "../../services/MailJet/saveEmail";

const crearToken = (usuarioLogin, secreto, expiresIn) => {
  const { usuario } = usuarioLogin;

  return jwt.sign({ usuario }, secreto, { expiresIn });
};

export const Mutation = {
  singleUpload(parent, { file }) {
    const matches = file.match(/^data:.+\/(.+);base64,(.*)$/);
    const ext = matches[1];
    const base64_data = matches[2];
    const buffer = new Buffer.from(base64_data, "base64");

    const filename = `${Date.now()}-file.${ext}`;
    const filenameWithPath = `${__dirname}/../../uploads/images/${filename}`;

    return new Promise((resolve, reject) => {
      fs.writeFile(filenameWithPath, buffer, (error) => {
        if (error) {
          reject(error);
          console.log(error);
        } else {
          resolve({ filename });
          console.log(filename);
        }
      });
    });
  },

  crearUsuario: async (root, { input }) => {
    SaveEmail(input.email, input.nombre);
    require("mongoose").model("usuarios").schema.add({
      isNotVerified: Boolean,
      verificationToken: String,
    });
    const token = crypto.randomBytes(20).toString("hex");
    // check if email exists
    const emailExists = await Usuarios.findOne({ email: input.email });
    if (emailExists) {
      return {
        success: false,
        message: "El Email Éxiste en la db",
        data: null,
      };
    }
    // check if username exists
    const usernameExists = await Usuarios.findOne({
      usuario: input.email,
    });
    if (usernameExists) {
      return {
        success: false,
        message: "El suario existe en la base de datos",
        data: null,
      };
    }
    const nuevoUsuario = await new Usuarios({
      nombre: input.nombre,
      apellidos: input.apellidos,
      email: input.email,
      password: input.password,
      verificationToken: token,
      isNotVerified: true,
    });
    nuevoUsuario.id = nuevoUsuario._id;

    const email = input.email;
    const transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      service: "gmail",
      port: 465,
      secure: true,
      auth: {
        user: process.env.EMAIL_ADDRESS,
        pass: `${process.env.EMAIL_PASSWORD}`,
      },
    });
    const mailOptions = {
      from: process.env.EMAIL_ADDRESS,
      to: email,
      subject: "Verifica tu correo electrónico de Vetec",
      text: `Verifica tu correo electrónico de Vetec`,
      html: confirmEmail(token),
    };

    await transporter.sendMail(mailOptions, (err, response) => {
      console.log(err);
    });

    return new Promise((resolve, object) => {
      nuevoUsuario.save((error) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else
          resolve({
            success: true,
            message: "Usuario agregado con éxito",
            data: nuevoUsuario,
          });
      });
    });
  },

  ////////////adimn ///////////////////

  crearadmin: async (root, { input }) => {
    require("mongoose")
      .model("profesionals")
      .schema.add({ verificationToken: String });
    const token = crypto.randomBytes(20).toString("hex");
    // check if email exists
    const emailExists = await Useradmin.findOne({
      email: input.email,
    });
    if (emailExists) {
      return {
        success: false,
        message: "El Email Éxiste en el sistema",
        data: null,
      };
    }
    // check if username exists
    const usernameExists = await Useradmin.findOne({
      email: input.email,
    });
    if (usernameExists) {
      return {
        success: false,
        message: "El suario existe en la base de datos",
        data: null,
      };
    }
    const nuevoUserAdmin = await new Useradmin({
      nombre: input.nombre,
      apellidos: input.apellidos,
      email: input.email,
      password: input.password,
    });
    nuevoUserAdmin.id = nuevoUserAdmin._id;
    return new Promise((resolve, object) => {
      nuevoUserAdmin.save((error) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else
          resolve({
            success: true,
            message: "Profesional agregado con éxito",
            data: nuevoUserAdmin,
          });
      });
    });
  },

  autenticaradmin: async (root, { email, password }) => {
    const emailpro = await Useradmin.findOne({ email: email });
    if (!emailpro) {
      return {
        success: false,
        message: "Aún no te has registrado como adminitrador se sistema",
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(password, emailpro.password);
    if (!passwordCorrecto) {
      return {
        success: false,
        message: "Contraseña no coinside",
        data: null,
      };
    }
    if (!emailpro.isAdmin === true) {
      return {
        success: false,
        message: "Aún no te has registrado como adminitrador de sistema",
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenido al equipo de Vetec!",
      data: {
        token: crearToken(emailpro, process.env.SECRETO, "2400hr"),
        id: emailpro._id,
      },
    };
  },

  actualizaradmin: async (root, { input }) => {
    return new Promise((resolve, reject) => {
      Useradmin.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error, _admin) => {
          if (error) reject(error);
          else resolve(_admin);
        }
      );
    });
  },

  eliminarAdmin: (root, { id }) => {
    return new Promise((resolve, object) => {
      Useradmin.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Miembro eliminado con éxito",
          });
      });
    });
  },

  eliminarCupon: (root, { id }) => {
    return new Promise((resolve, object) => {
      Cupones.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Miembro eliminado con éxito",
          });
      });
    });
  },

  eliminarconsulta: (root, { id }) => {
    return new Promise((resolve, object) => {
      Consulta.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Consulta eliminado con éxito",
          });
      });
    });
  },

  //////////////end//////////////////

  /////////// Professional seting ////////////

  crearProfesional: async (root, { input }) => {
    require("mongoose")
      .model("profesionals")
      .schema.add({ verificationToken: String });
    const token = crypto.randomBytes(20).toString("hex");
    // check if email exists
    const emailExists = await Profesional.findOne({
      email: input.email,
    });
    if (emailExists) {
      return {
        success: false,
        message: "El Email Éxiste en el sistema",
        data: null,
      };
    }
    // check if username exists
    const usernameExists = await Profesional.findOne({
      email: input.email,
    });
    if (usernameExists) {
      return {
        success: false,
        message: "El suario existe en la base de datos",
        data: null,
      };
    }
    const nuevoProfesional = await new Profesional({
      nombre: input.nombre,
      apellidos: input.apellidos,
      email: input.email,
      password: input.password,
    });
    nuevoProfesional.id = nuevoProfesional._id;
    return new Promise((resolve, object) => {
      nuevoProfesional.save((error) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else
          resolve({
            success: true,
            message: "Profesional agregado con éxito",
            data: nuevoProfesional,
          });
      });
    });
  },

  autenticarProfesional: async (root, { email, password }) => {
    const emailpro = await Profesional.findOne({ email: email });
    if (!emailpro) {
      return {
        success: false,
        message: "Aún no te has registrado como profesional",
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(password, emailpro.password);
    if (!passwordCorrecto) {
      return {
        success: false,
        message: "Contraseña no coinside",
        data: null,
      };
    }
    if (!emailpro.isProfesional === true) {
      return {
        success: false,
        message: "Aún no te has registrado como profesional",
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenido a Vetec PRO!",
      data: {
        token: crearToken(emailpro, process.env.SECRETO, "2400hr"),
        id: emailpro._id,
        verifyPhone: emailpro.verifyPhone,
      },
    };
  },

  eliminarProfesional: (root, { id }) => {
    return new Promise((resolve, object) => {
      Profesional.findOneAndDelete({ _id: id }, (error) => {
        if (error) {
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        } else {
          Consulta.deleteMany({ profesional: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          UsuarioFavoritoPro.deleteMany({
            profesionalId: id,
          })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          Conversation.deleteMany({ prof: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          Notification.deleteMany({ user: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          Notification.deleteMany({ profesional: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          ProfVisita.deleteMany({ profesional_id: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          Pago.deleteMany({ profesionalID: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          resolve({
            success: true,
            message: "Profesional eliminado con éxito",
          });
        }
      });
    });
  },

  actualizarProfesional: async (root, { input }) => {
    return new Promise((resolve, reject) => {
      Profesional.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error, _profeisonal) => {
          if (error) {
            console.log(error);
            reject(error);
          } else resolve(_profeisonal);
        }
      );
    });
  },

  ////////////// ENd /////////////////////

  eliminarUsuario: (root, { id }) => {
    return new Promise((resolve, object) => {
      Usuarios.findOneAndDelete({ _id: id }, (error) => {
        if (error) {
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        } else {
          Adoptar.deleteMany({ usuario: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          Consulta.deleteMany({ usuario: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          UsuarioFavoritoPro.deleteMany({
            usuarioId: id,
          })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          Mascota.deleteMany({ usuario: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          Conversation.deleteMany({ user: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          Notification.deleteMany({ user: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          Notification.deleteMany({ usuario: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
          resolve({
            success: true,
            message: "Usuario eliminado con éxito",
          });
        }
      });
    });
  },

  actualizarUsuario: async (root, { input }) => {
    return new Promise((resolve, reject) => {
      Usuarios.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error, _usuario) => {
          if (error) reject(error);
          else resolve(_usuario);
        }
      );
    });
  },

  autenticarUsuario: async (root, { email, password }) => {
    const emailUsuario = await Usuarios.findOne({ email: email });
    if (!emailUsuario) {
      return {
        success: false,
        message: "Aun no tenemos este email",
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(
      password,
      emailUsuario.password
    );
    if (!passwordCorrecto) {
      return {
        success: false,
        message: "Contraseña incorrecta, intentalo de nuevo ",
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenido a Vetec",
      data: {
        token: crearToken(emailUsuario, process.env.SECRETO, "2400hr"),
        id: emailUsuario._id,
        verifyPhone: emailUsuario.verifyPhone,
      },
    };
  },

  crearModificarConsulta: async (root, { input }, { usuarioActual }) => {
    try {
      if (!usuarioActual) {
        return {
          success: false,
          message: "Debes iniciar sesión para continiar",
          data: null,
        };
      }
      let consulta;
      if (input.id) {
        consulta = await Consulta.findById(ObjectId(input.id)).exec();
      }
      let cupon;
      let agotado;
      if (input.clave) {
        const cupons = await Cupones.findOne({ clave: input.clave }).exec();
        if (cupons) {
          cupon = cupons;
          agotado = await Consulta.findOne({
            usuario: ObjectId(input.usuario),
            cupon: ObjectId(cupons._id),
          }).exec();
        } else {
          cupon = null;
          agotado = null;
          return {
            success: false,
            message: "Cupón no valido, intenta con otra clave",
          };
        }
      }

      if (!consulta || !consulta._id) {
        consulta = new Consulta({
          endDate: input.endDate,
          time: input.time,
          cupon: cupon && cupon._id ? cupon._id : cupon,
          nota: input.nota,
          aceptaTerminos: input.aceptaTerminos,
          cantidad: input.cantidad || 1,
          usuario: input.usuario,
          profesional: input.profesional,
          mascota: input.mascota,
        });
      } else {
        if (!consulta.cupon) {
          if (agotado) {
            return {
              success: false,
              message: "Este cupón de descuento ya ha sido utilizado",
              data: null,
            };
          } else {
            consulta.set({ cupon: cupon ? cupon._id : null });
          }
        } else {
          if (agotado) {
            return {
              success: false,
              message: "Este cupón de descuento ya ha sido utilizado",
              data: null,
            };
          }
        }
      }
      return new Promise((resolve, reject) => {
        consulta.save((error, consultaguardada) => {
          if (error) {
            console.log(
              "resolvers -> mutation -> crearModificarOrden -> error1 ",
              error
            );
            return reject(error);
          } else {
            if (!consultaguardada.descuento && cupon && cupon._id) {
              consultaguardada.descuento = cupon;
            }
            return resolve({
              success: true,
              message: "Consulta realizada con éxito",
              data: consultaguardada,
            });
          }
        });
      });
    } catch (error) {
      console.log(
        "resolvers -> mutation -> crearModificarOrden -> error2 ",
        error
      );
      return new Promise((_resolve, reject) => {
        return reject({
          success: false,
          message: "Hay un problema con tu solicitud",
          data: null,
        });
      });
    }
  },

  crearCupon: async (root, { input }, { usuarioActual }) => {
    try {
      if (!usuarioActual) {
        return {
          success: false,
          message: "Debes iniciar sesión para continiar",
          data: null,
        };
      }
      const nuevoCupon = new Cupones({
        clave: input.clave,
        descuento: input.descuento,
        tipo: input.tipo,
      });
      return new Promise((resolve, reject) => {
        nuevoCupon.save((error, cupon) => {
          if (error) {
            console.log(
              "resolvers -> mutation -> crearCupon -> error1 ",
              error
            );
            return reject(error);
          } else {
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      console.log("resolvers -> mutation -> crearCupon -> error2 ", error);
      return new Promise((_resolve, reject) => {
        return reject({
          success: false,
          message: "Hay un problema con su solicitud",
          data: null,
        });
      });
    }
  },

  crearMascota: async (root, { input }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debes iniciar sesión para continiar",
        data: null,
      };
    }
    const usuario = await Usuarios.findOne({
      usuario: usuarioActual.usuario,
    });
    if (!usuario) {
      return {
        success: false,
        message: "Debes iniciar sesión para continiar",
        data: null,
      };
    }
    const nuevaMascota = await new Mascota({
      name: input.name,
      age: input.age,
      avatar: input.avatar,
      usuario: input.usuario,
      especie: input.especie,
      raza: input.raza,
      genero: input.genero,
      peso: input.peso,
      microchip: input.microchip,
      vacunas: input.vacunas,
      alergias: input.alergias,
      castrado: input.castrado,
      protectoraID: input.protectoraID,
      protectora: input.protectora,
      ciudad: input.ciudad,
      descripcion: input.descripcion,
    });
    nuevaMascota.id = nuevaMascota._id;
    return new Promise((resolve, reject) => {
      nuevaMascota.save((error) => {
        if (error)
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            data: null,
          });
        else
          resolve({
            success: true,
            message: "Mascota añadida con éxito",
            data: nuevaMascota,
          });
      });
    });
  },

  actualizarMascota: async (root, { input }) => {
    return new Promise((resolve, reject) => {
      Mascota.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error, mascota) => {
          console.log(mascota);
          if (error) {
            console.log(error);
            reject(error);
          } else
            resolve({
              success: true,
              message: "Datos actualizado con éxito",
            });
        }
      );
    });
  },

  crearDiagnostico: async (root, { input }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debes iniciar sesión para continiar",
        data: null,
      };
    }
    const usuario = await Usuarios.findOne({
      usuario: usuarioActual.usuario,
    });
    if (!usuario) {
      return {
        success: false,
        message: "Debes iniciar sesión para continiar",
        data: null,
      };
    }
    const nuevaDiagnostico = await new Diagnostico({
      profesional: {
        nombre: input.profesional.nombre,
        apellidos: input.profesional.apellidos,
        profesion: input.profesional.profesion,
        avatar: input.profesional.avatar,
      },
      diagnostico: input.diagnostico,
      mascota: input.mascota,
    });
    nuevaDiagnostico.id = nuevaDiagnostico._id;
    return new Promise((resolve, reject) => {
      nuevaDiagnostico.save((error) => {
        if (error)
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            data: null,
          });
        else
          resolve({
            success: true,
            message: "Diagnostico añadida con éxito",
            data: nuevaDiagnostico,
          });
      });
    });
  },

  eliminarMascota: (root, { id }, { usuarioActual }) => {
    return new Promise((resolve, object) => {
      Mascota.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          rejects({
            success: false,
            message: "Hay un problema con su solicitud",
          });
        else
          Adoptar.deleteMany({
            mascota: id,
          })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

        resolve({
          success: true,
          message: "Mascota eliminada con éxito",
        });
      });
    });
  },

  consultaProceed: async (
    root,
    { consultaID, estado, progreso, status, coment, rate, descripcionproblem },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debes iniciar sesión para continiar",
      };
    }

    const usuario = await Usuarios.findOne({
      usuario: usuarioActual.usuario,
    });
    if (!usuario) {
      return {
        success: false,
        message: "Debes iniciar sesión para continiar",
      };
    }

    return new Promise((resolve, reject) => {
      const message =
        estado === "Aceptado" ? "Consulta aceptada" : "Consulta rechazada";
      let dataToUpdate = { estado };
      if (progreso) dataToUpdate.progreso = progreso;
      if (descripcionproblem)
        dataToUpdate.descripcionproblem = descripcionproblem;
      if (coment) dataToUpdate.coment = coment;
      if (rate) dataToUpdate.rate = rate;
      if (status) dataToUpdate.status = status;
      Consulta.findOneAndUpdate({ _id: consultaID }, dataToUpdate, (error) => {
        if (error) {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
          });
        } else {
          resolve({
            success: true,
            message,
          });
        }
      });
    });
  },

  createNotification: async (root, { input }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debes iniciar sesión para continiar",
        data: null,
      };
    }
    let newNotification = new Notification({
      user: input.user,
      usuario: input.usuario,
      profesional: input.profesional,
      type: input.type,
      consulta: input.consulta,
    });
    return new Promise((resolve, reject) => {
      newNotification.save((error) => {
        if (error)
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Notificacion creada con éxito",
          });
      });
    });
  },

  readNotification: async (root, { notificationId }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debes iniciar sesión para continiar",
        data: null,
      };
    }

    return new Promise((resolve, reject) => {
      Notification.findOneAndUpdate(
        { _id: ObjectId(notificationId) },
        { $set: { read: true } },
        (error) => {
          if (error)
            reject({
              success: false,
              message: "Hay un problema con su solicitud",
            });
          else
            resolve({
              success: true,
              message: "Notificacion leída con éxito",
            });
        }
      );
    });
  },

  crearDeposito: async (root, { input }) => {
    const nuevoDeposito = await new Deposito({
      fecha: new Date(),
      estado: input.estado,
      total: input.total,
      profesionalID: input.profesionalID,
    });
    nuevoDeposito.id = nuevoDeposito._id;
    return new Promise((resolve, reject) => {
      nuevoDeposito.save((error) => {
        if (error)
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            data: null,
          });
        else
          resolve({
            success: true,
            message: "Deposito añadido con éxito",
            data: nuevoDeposito,
          });
      });
    });
  },

  eliminarDeposito: async (root, { id }) => {
    return new Promise((resolve, reject) => {
      Deposito.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Deposito eliminado con éxito",
          });
      });
    });
  },

  crearPago: async (root, { input }, { usuarioActual }) => {
    const nuevoPago = await new Pago({
      nombre: input.nombre,
      iban: input.iban,
      profesionalID: input.profesionalID,
    });

    nuevoPago.id = nuevoPago._id;

    return new Promise((resolve, reject) => {
      nuevoPago.save((error) => {
        if (error)
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            data: null,
          });
        else
          resolve({
            success: true,
            message: "Pago añadido con éxito",
            data: nuevoPago,
          });
      });
    });
  },

  eliminarPago: async (root, { id }) => {
    return new Promise((resolve, reject) => {
      Pago.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Pago eliminado con éxito",
          });
      });
    });
  },

  crearCategory: (root, { input }) => {
    const nuevoCategory = new Categories({
      title: input.title,
      image: input.image,
      description: input.description,
      font: input.font,
    });

    nuevoCategory.id = nuevoCategory._id;

    return new Promise((resolve, object) => {
      nuevoCategory.save((error) => {
        if (error) reject(error);
        else resolve(nuevoCategory);
      });
    });
  },

  readMessage: async (root, { conversationID }) => {
    return new Promise((resolve, reject) => {
      Conversation.updateMany(
        {
          _id: ObjectId(conversationID),
        },
        { $set: { "messagechat.$[].read": true } },
        { multi: true },
        (error) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un error con su solicitud",
            });
          else
            resolve({
              success: true,
              message: "Mesanjes leído con éxito",
            });
        }
      );
    });
  },

  recibeMessage: async (root, { conversationID }) => {
    return new Promise((resolve, reject) => {
      Conversation.updateMany(
        {
          _id: ObjectId(conversationID),
        },
        { $set: { "messagechat.$[].pending": false } },
        { multi: true },
        (error) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un error con su solicitud",
            });
          else
            resolve({
              success: true,
              message: "Mesanjes leído con éxito",
            });
        }
      );
    });
  },

  deleteConversation: async (root, { id }) => {
    return new Promise((resolve, reject) => {
      Conversation.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un error con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Conversacón eliminada con éxito",
          });
      });
    });
  },

  eliminarCategory: (root, { id }) => {
    return new Promise((resolve, object) => {
      Categories.findOneAndDelete({ _id: id }, (error) => {
        if (error) rejects(error);
        else resolve("Eliminado correctamente");
      });
    });
  },

  crearUsuarioFavoritoPro: async (
    root,
    { profesionalId, usuarioId },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debes iniciar sesión para comtinuar",
        data: null,
      };
    }

    const nuevoUsuarioFavoritoPro = await new UsuarioFavoritoPro({
      usuarioId,
      profesionalId,
    });
    nuevoUsuarioFavoritoPro.id = nuevoUsuarioFavoritoPro._id;
    return new Promise((resolve, reject) => {
      nuevoUsuarioFavoritoPro.save((error) => {
        if (error)
          reject({
            success: false,
            message: "Hay un problema con tu solicitud",
          });
        else
          resolve({
            success: true,
            message: "Profesional añadido a favorito",
          });
      });
    });
  },

  eliminarUsuarioFavoritoPro: async (root, { id }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debes iniciar sesión para comtinuar",
        data: null,
      };
    }
    return new Promise((resolve, reject) => {
      UsuarioFavoritoPro.findOneAndDelete({ profesionalId: id }, (error) => {
        if (error)
          reject({
            success: false,
            message: "Hay un problema con tu solicitud",
          });
        else
          resolve({
            success: true,
            message: "Profesional eliminado de favorito",
          });
      });
    });
  },

  crearProtectora: async (root, { input }) => {
    console.log(input);
    // check if email exists
    const emailExists = await Protectora.findOne({ email: input.email });
    if (emailExists) {
      return {
        success: false,
        message: "El email éxiste en la en nuestros sistema",
      };
    }
    const nuevoprotectora = await new Protectora({
      name: input.name,
      email: input.email,
      password: input.password,
      phone: input.phone,
    });
    nuevoprotectora.id = nuevoprotectora._id;

    return new Promise((resolve, reject) => {
      nuevoprotectora.save((error) => {
        console.log(error);
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Protectora agregada con éxito",
          });
      });
    });
  },

  autenticarProtectora: async (root, { email, password }) => {
    const emailpro = await Protectora.findOne({ email: email });
    if (!emailpro) {
      return {
        success: false,
        message: "Aún no te has registrado como proctectora de Vetec",
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(password, emailpro.password);
    if (!passwordCorrecto) {
      return {
        success: false,
        message: "La contraseña no coinside",
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenido al equipo de Vetec para protectora!",
      data: {
        token: crearToken(emailpro, process.env.SECRETO, "2400hr"),
        id: emailpro._id,
      },
    };
  },

  eliminarProtectora: (root, { id }) => {
    return new Promise((resolve, object) => {
      Protectora.findOneAndDelete({ _id: id }, (error) => {
        if (error) {
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        } else {
          Adoptar.deleteMany({ protectora: id })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });

          Mascota.deleteMany({
            protectoraID: id,
            protectora: true,
          })
            .then(function () {
              console.log("Data deleted"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
          resolve({
            success: true,
            message: "Protectora eliminado con éxito",
          });
        }
      });
    });
  },

  actualizarProtectora: async (root, { input }) => {
    return new Promise((resolve, reject) => {
      Protectora.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error, protectora) => {
          if (error) reject(error);
          else
            resolve({
              success: true,
              message: "Datos actualizado con éxito",
            });
        }
      );
    });
  },

  crearSolititud: async (root, { input }) => {
    const nuevoadoptar = await new Adoptar({
      name: input.name,
      lastName: input.lastName,
      email: input.email,
      city: input.city,
      phone: input.phone,
      experiencia: input.experiencia,
      otra: input.otra,
      fecha: input.fecha,
      nino: input.nino,
      mascota: input.mascota,
      usuario: input.usuario,
      protectora: input.protectora,
      estado: input.estado,
      UserID: input.UserID,
    });
    nuevoadoptar.id = nuevoadoptar._id;

    return new Promise((resolve, reject) => {
      nuevoadoptar.save((error) => {
        console.log(error);
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Solicitud creada con éxito",
          });
      });
    });
  },

  actualizarsolicitud: async (root, { input }) => {
    return new Promise((resolve, reject) => {
      Adoptar.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error) => {
          if (error) reject(error);
          else
            resolve({
              success: true,
              message: "Solicitud actualizado con éxito",
            });
        }
      );
    });
  },
};
