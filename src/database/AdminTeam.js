var bcrypt = require("bcryptjs");
var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const adminSchema = new mongoose.Schema(
    {
      nombre: { type: String, text: true },
      apellidos: { type: String, text: true },
      email: String,
      ciudad: String,
      telefono: String,
      avatar: String,
      password: String,
      UserID: String,
      cargo: String,
      descricion: String,
      twitter: String,
      linkedin: String,
      createBy: { type: mongoose.Schema.Types.ObjectId },
      notificacion: { type: mongoose.Schema.Types.Boolean, default: false },
      isAdmin: { type: mongoose.Schema.Types.Boolean, default: true },
      isAvailable: { type: mongoose.Schema.Types.Boolean, default: false },
    },
    { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
  );
  
  // hashear los password antes de guardar
  adminSchema.pre("save", function (next) {
    // Si el password no esta hasheado...
    if (!this.isModified("password")) {
      return next();
    }
    bcrypt.genSalt(10, (err, salt) => {
      if (err) return next(err);
  
      bcrypt.hash(this.password, salt, (err, hash) => {
        if (err) return next(err);
        this.password = hash;
        next();
      });
    });
  });
  
const Useradmin = mongoose.model("adminuser", adminSchema);

module.exports = Useradmin;