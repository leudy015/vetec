"use strict";
import {
  FACEBOOK_APP_ID,
  GOOGLE_CLIENT_ID,
  google_customer_secret,
  twiterr_api_key,
  twitter_api_secret,
  FACEBOOK_SECRET,
} from "./config.js";
import bcrypt from "bcryptjs";
import crypto from "crypto";
import { Usuarios } from "../database";
var passport = require("passport");
var TwitterTokenStrategy = require("passport-twitter-token");
var FacebookTokenStrategy = require("passport-facebook-token");
var GoogleTokenStrategy = require("passport-google-token").Strategy;
import confirmEmail from "../EmailSistems/ConfirmEmail";
const nodemailer = require("nodemailer");
import { SaveEmail } from "../../services/MailJet/saveEmail";

module.exports = function () {
  passport.use(
    new TwitterTokenStrategy(
      {
        consumerKey: twiterr_api_key,
        consumerSecret: twitter_api_secret,
        includeEmail: true,
      },
      async (token, tokenSecret, profile, done) => {
        const mail = profile.emails[0].value;
        const name =
          profile.name.givenName.length > 0
            ? profile.name.givenName
            : profile.username;
        SaveEmail(mail, name);
        require("mongoose").model("usuarios").schema.add({ isSocial: Boolean });

        const tokens = crypto.randomBytes(20).toString("hex");

        // // check if email exists
        let emailExists = await Usuarios.findOne({
          email: profile.emails[0].value,
        });
        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) console.log(err);

            bcrypt.hash(token, salt, (err, hash) => {
              if (err) console.log(err);
              Usuarios.findOneAndUpdate(
                { email: profile.emails[0].value },
                { password: hash },
                (err, updated) => {
                  if (err) console.log(err);

                  let nuevoUsuario = updated;
                  return done(err, { nuevoUsuario, token: token });
                }
              );
            });
          });
        } else {
          const nuevoUsuario = new Usuarios({
            id: profile.id,
            avatar: profile.photos[0].value,
            password: token,
            email: profile.emails[0].value,
            nombre:
              profile.name.givenName.length > 0
                ? profile.name.givenName
                : profile.username,
            apellidos: profile.name.familyName,
            isSocial: true,
            verificationToken: tokens,
            isNotVerified: true,
          });

          nuevoUsuario.id = nuevoUsuario._id;

          const email = profile.emails[0].value;
          const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            service: "gmail",
            port: 465,
            secure: true,
            auth: {
              user: process.env.EMAIL_ADDRESS,
              pass: `${process.env.EMAIL_PASSWORD}`,
            },
          });
          const mailOptions = {
            from: process.env.EMAIL_ADDRESS,
            to: email,
            subject: "Verifica tu correo electrónico de Vetec",
            text: `Verifica tu correo electrónico de Vetec`,
            html: confirmEmail(tokens),
          };

          await transporter.sendMail(mailOptions, (err, response) => {
            console.log(err);
          });

          nuevoUsuario.save((error) => {
            return done(error, { nuevoUsuario, token: token });
          });
        }
      }
    )
  );

  passport.use(
    new FacebookTokenStrategy(
      {
        clientID: FACEBOOK_APP_ID,
        clientSecret: FACEBOOK_SECRET,
      },
      async (accessToken, refreshToken, profile, done) => {
        const mail = profile.emails[0].value;
        const name = profile.name.givenName;
        SaveEmail(mail, name);

        require("mongoose").model("usuarios").schema.add({ isSocial: Boolean });

        const tokens = crypto.randomBytes(20).toString("hex");

        let emailExists = await Usuarios.findOne({
          email: profile.emails[0].value,
        });

        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) console.log(err);

            bcrypt.hash(accessToken, salt, (err, hash) => {
              if (err) console.log(err);
              Usuarios.findOneAndUpdate(
                { email: profile.emails[0].value },
                { password: hash },
                (err, updated) => {
                  if (err) console.log(err);

                  let nuevoUsuario = updated;
                  return done(err, { nuevoUsuario, token: accessToken });
                }
              );
            });
          });
        } else {
          const nuevoUsuario = await new Usuarios({
            id: profile.id,
            password: accessToken,
            email: profile.emails[0].value,
            avatar: profile.photos[0].value,
            nombre: profile.name.givenName,
            apellidos: profile.name.familyName,
            isSocial: true,
            verificationToken: tokens,
            isNotVerified: true,
          });
          nuevoUsuario.id = nuevoUsuario._id;

          const email = profile.emails[0].value;
          const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            service: "gmail",
            port: 465,
            secure: true,
            auth: {
              user: process.env.EMAIL_ADDRESS,
              pass: `${process.env.EMAIL_PASSWORD}`,
            },
          });
          const mailOptions = {
            from: process.env.EMAIL_ADDRESS,
            to: email,
            subject: "Verifica tu correo electrónico de Vetec",
            text: `Verifica tu correo electrónico de Vetec`,
            html: confirmEmail(tokens),
          };

          await transporter.sendMail(mailOptions, (err, response) => {
            console.log(err);
          });

          nuevoUsuario.save((error) => {
            return done(error, { nuevoUsuario, token: accessToken });
          });
        }
      }
    )
  );

  passport.use(
    new GoogleTokenStrategy(
      {
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: google_customer_secret,
      },
      async (accessToken, refreshToken, profile, done) => {
        const mail = profile.emails[0].value;
        const name = profile.name.givenName;
        SaveEmail(mail, name);

        require("mongoose").model("usuarios").schema.add({ isSocial: Boolean });

        const tokens = crypto.randomBytes(20).toString("hex");

        // check if email exists
        let emailExists = await Usuarios.findOne({
          email: profile.emails[0].value,
        });

        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) console.log(err);

            bcrypt.hash(accessToken, salt, (err, hash) => {
              if (err) console.log(err);

              Usuarios.findOneAndUpdate(
                { email: profile.emails[0].value },
                { password: hash },
                (err, updated) => {
                  if (err) console.log(err);

                  console.log(updated);
                  let nuevoUsuario = updated;
                  return done(err, { nuevoUsuario, token: accessToken });
                }
              );
            });
          });
        } else {
          const nuevoUsuario = await new Usuarios({
            id: profile.id,
            password: accessToken,
            email: profile.emails[0].value,
            avatar: profile.photos[0].value,
            nombre: profile.name.givenName,
            apellidos: profile.name.familyName,
            isSocial: true,
            verificationToken: tokens,
            isNotVerified: true,
          });
          nuevoUsuario.id = nuevoUsuario._id;

          const email = profile.emails[0].value;
          const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            service: "gmail",
            port: 465,
            secure: true,
            auth: {
              user: process.env.EMAIL_ADDRESS,
              pass: `${process.env.EMAIL_PASSWORD}`,
            },
          });
          const mailOptions = {
            from: process.env.EMAIL_ADDRESS,
            to: email,
            subject: "Verifica tu correo electrónico de Vetec",
            text: `Verifica tu correo electrónico de Vetec`,
            html: confirmEmail(tokens),
          };

          await transporter.sendMail(mailOptions, (err, response) => {
            console.log(err);
          });

          nuevoUsuario.save((error) => {
            return done(error, { nuevoUsuario, token: accessToken });
          });
        }
      }
    )
  );
};
