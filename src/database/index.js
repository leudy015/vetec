var bcrypt = require("bcryptjs");
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
var mongoose = require("mongoose");

mongoose
  .connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("La conexi�n a MongoDB se ha realizado correctamente!!");
  })
  .catch((err) => console.log(err));

mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);

mongoose.Promise = global.Promise;

const Usuarios = require("./User");
const Consulta = require("./Consultas");
const Mascota = require("./Mascotas");
const Adoptar = require("./Adopcion");
const Profesional = require("./Profesionales");
const Useradmin = require("./AdminTeam");
const Cupones = require("./Cupones");
const Notification = require("./Notifications");
const Conversation = require("./Conversaciones");
const Diagnostico = require("./Diagnostico");
const Deposito = require("./deposito");
const Pago = require("./pago");
const Categories = require("./Category");
const UsuarioFavoritoPro = require("./FavouritesVet");
const ProfVisita = require("./Visitacount");
const Protectora = require("./protectora");

export {
  Usuarios,
  Consulta,
  Mascota,
  Profesional,
  Cupones,
  Notification,
  Conversation,
  Diagnostico,
  Deposito,
  Pago,
  Useradmin,
  Categories,
  UsuarioFavoritoPro,
  ProfVisita,
  Adoptar,
  Protectora,
};
