const express = require("express");
import path from "path";
const http = require("http");
import { Types } from "mongoose";
const { ObjectId } = Types;
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
const app = express();
const server = http.createServer(app);
const { ApolloServer } = require("apollo-server-express");
import { typeDefs } from "./GraphQL/schema";
import { resolvers } from "./GraphQL/resolvers";
var socketIO = require("socket.io");
var cors = require("cors");
const { Storage } = require("@google-cloud/storage");
var bcrypt = require("bcryptjs");
const request = require("request");
import { twiterr_api_key, twitter_api_secret } from "./LoginSocial/config";
var passport = require("passport");
require("./LoginSocial/passport")();
import { Consulta, Conversation, Usuarios } from "./database";
const paypal = require("paypal-rest-sdk");
const engines = require("consolidate");
import { SaveEmail } from "../services/MailJet/saveEmail";

app.engine("ejs", engines.ejs);
app.set("views", "views");
app.set("view engine", "ejs");

const port = 3000;

const whitelist = [
  "https://vetec.es",
  "https://www.vetec.es",
  "https://profesionales.vetec.es",
  "https://dashboard.vetec.es",
];

const corsOptions = {
  origin: (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      //callback(new Error('Not allowed by CORS'));
      callback(null, true);
    }
  },
  optionsSuccessStatus: 200,
};
const bodyParser = require("body-parser");
app.use(cors(corsOptions));

app.get("/", (req, res) => res.send("Hello Vetec!"));
app.use(require("../services/Stripe/rauterStripe"));
app.use(require("../services/Twilio/sendSMS"));
app.use(require("../services/Onesignal/index"));
app.use(require("./RecoveryPassword/index"));
app.use(require("../services/MailJet/index"));
app.use(require("./TensorFlow/index"));

app.use(
  bodyParser.urlencoded({
    parameterLimit: 100000,
    limit: "50mb",
    extended: true,
  })
);
app.use(bodyParser.json({ limit: "50mb", type: "application/json" }));

app.use("/assets", express.static(path.join(__dirname + "/../uploads")));

var createToken = function (auth) {
  return jwt.sign(
    {
      id: auth.id,
    },
    process.env.SECRETO,
    {
      expiresIn: 60 * 120,
    }
  );
};

function generateToken(req, res, next) {
  req.token = createToken(req.auth);
  return next();
}

function sendToken(req, res) {
  res.setHeader("x-auth-token", req.token);
  return res.status(200).send(JSON.stringify(req.user));
}

app.post("/api/v1/auth/twitter/reverse", function (req, res) {
  request.post(
    {
      url: "https://api.twitter.com/oauth/request_token",
      oauth: {
        oauth_callback: "https%3A%2F%2Flocalhost%3A3000%2Flogin",
        consumer_key: twiterr_api_key,
        consumer_secret: twitter_api_secret,
      },
    },
    function (err, r, body) {
      if (err) {
        return res.send(500, { message: e.message });
      }
      var jsonStr =
        '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
      res.send(JSON.parse(jsonStr));
    }
  );
});

app.post(
  "/api/v1/auth/twitter",
  (req, res, next) => {
    request.post(
      {
        url: `https://api.twitter.com/oauth/access_token?oauth_verifier`,
        oauth: {
          consumer_key: twiterr_api_key,
          consumer_secret: twitter_api_secret,
          token: req.query.oauth_token,
        },
        form: { oauth_verifier: req.query.oauth_verifier },
      },
      function (err, r, body) {
        if (err) {
          return res.send(500, { message: err.message });
        }

        const bodyString =
          '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
        const parsedBody = JSON.parse(bodyString);

        req.body["oauth_token"] = parsedBody.oauth_token;
        req.body["oauth_token_secret"] = parsedBody.oauth_token_secret;
        req.body["user_id"] = parsedBody.user_id;

        next();
      }
    );
  },
  passport.authenticate("twitter-token", { session: false }),
  function (req, res, next) {
    if (!req.user) {
      return res.send(401, "User Not Authenticated");
    }
    req.auth = {
      id: req.user.id,
    };

    return next();
  },
  generateToken,
  sendToken
);

app.post(
  "/api/v1/auth/facebook",
  passport.authenticate("facebook-token", { session: false }),
  function (req, res, next) {
    if (!req.user) {
      return res.send(401, "User Not Authenticated");
    }
    req.auth = {
      id: req.user.id,
    };

    next();
  },
  generateToken,
  sendToken
);

app.post(
  "/api/v1/auth/google",
  passport.authenticate("google-token", { session: false }),
  function (req, res, next) {
    if (!req.user) {
      return res.send(401, "User Not Authenticated");
    }
    req.auth = {
      id: req.user.id,
    };

    next();
  },
  generateToken,
  sendToken
);

app.post("/api/v1/auth/social/mobile", async function (req, res, next) {
  let data = req.body;
  SaveEmail(data.email, data.firstName);
  // // check if email exists
  let emailExists = await Usuarios.findOne({
    email: data.email,
  });

  if (emailExists) {
    bcrypt.genSalt(10, (err, salt) => {
      if (err) console.log(err);
      bcrypt.hash(data.token, salt, (err, hash) => {
        if (err) console.log(err);
        Usuarios.findOneAndUpdate(
          { email: data.email },
          { password: hash },
          (err, updated) => {
            if (err) {
              res.status(500).json({ error: err });
            }
            let nuevoUsuario = updated;
            res.json({ nuevoUsuario, token: data.token });
          }
        );
      });
    });
  } else {
    const nuevoUsuario = new Usuarios({
      nombre: data.firstName,
      apellidos: data.lastName,
      email: data.email,
      password: data.token,
      isSocial: true,
    });

    nuevoUsuario.id = nuevoUsuario._id;

    nuevoUsuario.save((error) => {
      if (error) {
        return res.status(500).send({ error });
      } else {
        return res.json({ nuevoUsuario, token: data.token });
      }
    });
  }
});

app.post("/tokenValidation", (req, res) => {
  Usuarios.find({ forgotPasswordToken: req.body.token }, (err, data) => {
    if (err || data.length < 1) {
      if (err) console.log(err);
      res.status(200).json({ isValid: false, email: "" });
    } else if (data.length > 0) {
      res.status(200).json({ isValid: true, email: data[0].email });
    }
  });
});

app.post("/resetPassword", cors(corsOptions), (req, res) => {
  console.log(req.body);
  var newvalues = { $unset: { forgotPasswordToken: req.body.token } };
  Usuarios.findOneAndUpdate(
    { email: req.body.email },
    newvalues,
    {
      //options
      returnNewDocument: true,
      new: true,
      strict: false,
      useFindAndModify: false,
    },
    (err, updated) => {
      if (err) console.log(err);
      bcrypt.genSalt(10, (err, salt) => {
        if (err) console.log(err);

        bcrypt.hash(req.body.password, salt, (err, hash) => {
          if (err) console.log(err);
          var newvalues2 = { $set: { password: hash } };
          Usuarios.findOneAndUpdate(
            { email: req.body.email },
            newvalues2,
            {
              //options
              returnNewDocument: true,
              new: true,
              strict: false,
              useFindAndModify: false,
            },
            (err, updated) => {
              if (err) console.log(err);

              console.log(updated);
              res.status(200).json({ changed: true });
            }
          );
        });
      });
    }
  );
});

app.post("/tokenValidation-profesional", (req, res) => {
  Profesional.find({ forgotPasswordToken: req.body.token }, (err, data) => {
    if (err || data.length < 1) {
      if (err) console.log(err);
      res.status(200).json({ isValid: false, email: "" });
    } else if (data.length > 0) {
      res.status(200).json({ isValid: true, email: data[0].email });
    }
  });
});

app.post("/resetPassword-profesional", cors(corsOptions), (req, res) => {
  var newvalues = { $unset: { forgotPasswordToken: req.body.token } };

  Profesional.findOneAndUpdate(
    { email: req.body.email },
    newvalues,
    {
      //options
      returnNewDocument: true,
      new: true,
      strict: false,
      useFindAndModify: false,
    },
    (err, updated) => {
      if (err) console.log(err);
      bcrypt.genSalt(10, (err, salt) => {
        if (err) console.log(err);

        bcrypt.hash(req.body.password, salt, (err, hash) => {
          if (err) console.log(err);
          var newvalues2 = { $set: { password: hash } };
          Profesional.findOneAndUpdate(
            { email: req.body.email },
            newvalues2,
            {
              //options
              returnNewDocument: true,
              new: true,
              strict: false,
              useFindAndModify: false,
            },
            (err, updated) => {
              if (err) console.log(err);

              console.log(updated);
              res.status(200).json({ changed: true });
            }
          );
        });
      });
    }
  );
});

//Middleware apollo server
const servergraphql = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const token = req.headers["authorization"];
    if (token !== "null") {
      try {
        // verificarl el token que viene del cliente
        const usuarioActual = await jwt.verify(token, process.env.SECRETO);
        req.usuarioActual = usuarioActual;

        return {
          usuarioActual,
        };
      } catch (err) {
        // console.log('err: ', err);
      }
    }
  },
});

servergraphql.applyMiddleware({ app });

app.use(function (req, res, next) {
  //to allow cross domain requests to send cookie information.
  res.setHeader("Access-Control-Allow-Credentials", true);

  // origin can not be '*' when crendentials are enabled. so need to set it to the request origin
  res.setHeader("Access-Control-Allow-Origin", "*");

  // list of methods that are supported by the server
  res.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");

  res.setHeader(
    "Access-Control-Allow-Headers",
    "Authorization, Origin, X-Requested-With, Content-Type, Accept"
  );

  next();
});

//////// stripe ///////////

const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);

app.post("/create-card", async (req, res) => {
  const { customer, paymentMethod } = req.body;
  const paymentMethods = await stripe.paymentMethods.attach(paymentMethod.id, {
    customer: customer,
  });

  if (paymentMethods) {
    res
      .status(200)
      .send({ success: true, data: "Método de pago añadido con éxito" });
  } else {
    res
      .status(404)
      .send({ success: false, data: "Algo salio mas intentalo de nuevo" });
  }
});

//////////////////////////

///////////paypal ///////////

paypal.configure({
  mode: "live", //sandbox or live
  client_id: process.env.PAYPALCLIENTID,
  client_secret: process.env.PAYPALCLIENTSECRET,
});

app.get("/paypal", (req, res) => {
  const { price, order } = req.query;
  var create_payment_json = {
    intent: "sale",
    payer: {
      payment_method: "paypal",
    },
    redirect_urls: {
      return_url: `https://server.vetec.es/success?price=${price}&order=${order}`,
      cancel_url: "https://server.vetec.es/cancel",
    },
    transactions: [
      {
        amount: {
          currency: "EUR",
          total: req.query.price,
        },
        description: "Vetec App S.L",
      },
    ],
  };

  paypal.payment.create(create_payment_json, function (error, payment) {
    if (error) {
      throw error;
    } else {
      console.log("Create Payment Response");
      console.log(payment);
      res.redirect(payment.links[1].href);
    }
  });
});

app.get("/success", (req, res) => {
  var PayerID = req.query.PayerID;
  var paymentId = req.query.paymentId;
  const { price, order } = req.query;
  var execute_payment_json = {
    payer_id: PayerID,
    transactions: [
      {
        amount: {
          currency: "EUR",
          total: price,
        },
      },
    ],
  };

  paypal.payment.execute(paymentId, execute_payment_json, function (
    error,
    payment
  ) {
    if (error) {
      console.log(error.response);
      throw error;
    }
    if (payment) {
      Consulta.findOneAndUpdate(
        { _id: ObjectId(order) },
        {
          $set: {
            estado: "Nueva",
            progreso: "50",
            status: "active",
            pagoPaypal: payment,
          },
        },
        (err, order) => {
          res.render("success");
        }
      );
      console.log("Get Payment Response");
    }
  });
});

app.get("/cancel", (req, res) => {
  res.render("cancel");
});
/////////////////////////

////////////Socket conection y manage and DialogoFlow ///////////////

const projectId = process.env.GOOGLEPROJECTID;
const keyFilename = process.env.GOOGLE_APPLICATION_CREDENTIALS;

const storage = new Storage({ projectId, keyFilename });

async function authenticate() {
  try {
    // Makes an authenticated API request.
    const results = await storage.getBuckets();

    const [buckets] = results;

    console.log("Buckets:");
    buckets.forEach((bucket) => {
      console.log(bucket.name);
    });
  } catch (err) {}
}

authenticate();

const io = socketIO(server);
let connectedUsers = {};

///here makes the connection
io.on("connection", (socket) => {
  ///////here you get the db messages
  socket.on("online_user", (id) => {
    connectedUsers[id.id] = socket.id;
    console.log(`new user connected with an id of ${id.id}`);
    Conversation.findOne({ user: id.usuario, prof: id.profesional })
      .sort({ $natural: -1 })
      .exec((err, res) => {});
  });

  socket.on("not_available", (data) => {
    Conversation.findOneAndUpdate(
      { user: data.user, prof: data.prof },
      { $set: { available: false } },
      (error) => {
        if (error) console.log(error);
        else console.log("actualizado");
      }
    );
  });

  socket.on("available", (data) => {
    Conversation.findOneAndUpdate(
      { user: data.user, prof: data.prof },
      { $set: { available: true } },
      (error) => {
        if (error) console.log(error);
        else console.log("actualizado");
      }
    );
  });

  //////here he hears the client's message and issues the specified user
  socket.on("private_message", (input) => {
    console.log(">>>>>>>>>>", input, input.receptor);
    io.to(connectedUsers[input.receptor]).emit("messages", input.messagechat);
    Conversation.find({ user: input.user, prof: input.prof }).exec(
      (err, res) => {
        if (res.length === 0) {
          let newMessageChat = new Conversation({
            messagechat: input.messagechat,
            prof: input.prof,
            user: input.user,
          });
          //////////here save in db and send to specific user
          newMessageChat.save((err, res) => {
            if (err) throw err;
          });
        } else {
          Conversation.updateOne(
            { _id: res[0]._id },
            {
              $push: {
                messagechat: { $each: input.messagechat, $position: 0 },
              },
              upsert: true,
              new: true,
            }
          ).exec((err, conversaciones) => {
            if (err) {
              console.log(err);
            }
          });
        }
      }
    );

    //////////Send Notification////////

    if (input.receptor === input.user) {
      const datos = input.messagechat;
      if (datos[0]) {
        var sendNotification = function (data) {
          var headers = {
            "Content-Type": "application/json; charset=utf-8",
          };
          var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers,
          };
          var https = require("https");
          var req = https.request(options, function (res) {
            res.on("data", function (data) {
              console.log("Response:");
            });
          });

          req.on("error", function (e) {
            console.log("ERROR:");
            console.log(e);
          });

          req.write(JSON.stringify(data));
          req.end();
        };

        var message = {
          app_id: "e461eb6c-b20a-4a47-8923-ff44868ed8b2",
          headings: { en: datos[0].user.name },
          contents: { en: datos[0].text },
          headings: { en: datos[0].user.name },
          contents: { en: datos[0].text },
          ios_badgeCount: 1,
          ios_badgeType: "Increase",
          include_player_ids: [input.Userid],
        };
        sendNotification(message);
      } else {
        return null;
      }
    } else {
      const datos = input.messagechat;
      if (datos[0]) {
        var sendNotification = function (data) {
          var headers = {
            "Content-Type": "application/json; charset=utf-8",
          };
          var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers,
          };
          var https = require("https");
          var req = https.request(options, function (res) {
            res.on("data", function (data) {
              console.log("Response:");
            });
          });

          req.on("error", function (e) {
            console.log("ERROR:");
            console.log(e);
          });

          req.write(JSON.stringify(data));
          req.end();
        };

        var message = {
          app_id: "3b850d4c-457b-4e1b-bc7d-e5e4dab86b27",
          headings: { en: datos[0].user.name },
          contents: { en: datos[0].text },
          headings: { en: datos[0].user.name },
          contents: { en: datos[0].text },
          ios_badgeCount: 1,
          ios_badgeType: "Increase",
          include_player_ids: [input.Userid],
        };

        sendNotification(message);
      } else {
        return null;
      }
    }
  });

  /////////////disconnect user///////////////////
  socket.on("disconnect", () => {
    console.log("User disconnect:", socket.id);
  });
});

server.listen(port, () =>
  console.log(
    `Server listen in por http://localhost:${port}${servergraphql.graphqlPath}`
  )
);
