var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const notificacionSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId },
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: "usuarios" },
    profesional: { type: mongoose.Schema.Types.ObjectId, ref: "profesionals" },
    type: { type: mongoose.Schema.Types.String },
    read: { type: mongoose.Schema.Types.Boolean, default: false },
    createdAt: { type: mongoose.Schema.Types.Date, default: Date.now },
    consulta: { type: mongoose.Schema.Types.ObjectId, ref: "consultas" },
  });
const Notification = mongoose.model("notification", notificacionSchema);

module.exports = Notification;