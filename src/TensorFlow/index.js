const express = require("express");
const tendorRouter = express.Router();
import path from "path";
const mobilenet = require('@tensorflow-models/mobilenet');
const cocoSsd = require('@tensorflow-models/coco-ssd');
const fs = require('fs');
const tfnode = require('@tensorflow/tfjs-node');
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
var cors = require("cors");


const whitelist = [
    "https://vetec.es",
    "https://www.vetec.es",
    "https://profesionales.vetec.es",
    "https://dashboard.vetec.es",
  ];
  
  const corsOptions = {
    origin: (origin, callback) => {
      if (whitelist.indexOf(origin) !== -1) {
        callback(null, true);
      } else {
        //callback(new Error('Not allowed by CORS'));
        callback(null, true);
      }
    },
    optionsSuccessStatus: 200,
  };


tendorRouter.get("/image-detection", cors(corsOptions), async (req, res) => {
    const { filename } = req.query;
    const IMG = path.join(__dirname + `/../../uploads/images/${filename}`);
  
    const readImage = path => {
      const imageBuffer = fs.readFileSync(path);
      const tfimage = tfnode.node.decodeImage(imageBuffer);
      return tfimage;
    }
  
    const imageClassification = async path => {
      const image = readImage(path);
      const mobilenetModel = await mobilenet.load();
      const predictions = await mobilenetModel.classify(image);
      console.log('Classification Results:', predictions[0]);
      res.json(predictions);
    }
  
    imageClassification(IMG);
  });
  
  tendorRouter.get("/object-detection", cors(corsOptions), async (req, res) => {
    const { filename } = req.query;
    const IMG = path.join(__dirname + `/../../uploads/images/${filename}`);  
  
    const readImage = path => {
      const imageBuffer = fs.readFileSync(path);
      const tfimage = tfnode.node.decodeImage(imageBuffer);
      return tfimage;
    }
  
    const imageClassification = async path => {
      const image = readImage(path);
      const mobilenetModel = await cocoSsd.load();
      const predictions = await mobilenetModel.detect(image);
      console.log('Classification Results:', predictions[0]);
      res.json(predictions);
    }
    imageClassification(IMG);
  });


  module.exports = tendorRouter;