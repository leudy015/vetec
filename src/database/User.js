var bcrypt = require("bcryptjs");
var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const usuariosSchema = new mongoose.Schema(
  {
    nombre: { type: String, text: true },
    apellidos: { type: String, text: true },
    email: { type: String, text: true },
    ciudad: { type: String, text: true },
    telefono: { type: String, text: true },
    avatar: String,
    password: String,
    UserID: String,
    setVideoConsultas: {
      type: Number,
      default: 0,
      maxlength: 3,
    },
    verifyPhone: { type: mongoose.Schema.Types.Boolean, default: false },
    MyVet: { type: mongoose.Schema.Types.ObjectId, ref: "profesionals" },
    isPlus: { type: mongoose.Schema.Types.Boolean, default: false },
    connected: { type: mongoose.Schema.Types.Boolean, default: false },
    lastTime: Date,
    StripeID: String,
    suscription: { type: mongoose.Schema.Types.Mixed },
    notificacion: { type: mongoose.Schema.Types.Boolean, default: false },
    isNotVerified: { type: mongoose.Schema.Types.Boolean, default: true },
    verificationToken: String,
    isSocial: Boolean,
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

// hashear los password antes de guardar
usuariosSchema.pre("save", function (next) {
  // Si el password no esta hasheado...
  if (!this.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(this.password, salt, (err, hash) => {
      if (err) return next(err);
      this.password = hash;
      next();
    });
  });
});

const Usuarios = mongoose.model("usuarios", usuariosSchema);

module.exports = Usuarios;
