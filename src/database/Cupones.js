var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const cuponesSchema = new mongoose.Schema({
    clave: { type: String, unique: true },
    descuento: Number,
    tipo: {
      type: String,
      enum: ["porcentaje", "dinero"],
      default: "porcentaje",
    },
  });
  
const Cupones = mongoose.model("cupones", cuponesSchema);

module.exports = Cupones;