var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const conversationsSchema = new mongoose.Schema(
    {
      user: { type: mongoose.Schema.Types.ObjectId, ref: "usuarios" },
      prof: { type: mongoose.Schema.Types.ObjectId, ref: "profesionals" },
      available: { type: mongoose.Schema.Types.Boolean, default: true },
      messagechat: [{ type: mongoose.Schema.Types.Mixed }],
    },
    { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
  );
const Conversation = mongoose.model("conversations", conversationsSchema);

module.exports = Conversation;