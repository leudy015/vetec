const express = require("express");
const onesignalRouter = express.Router();
import { Types } from "mongoose";
const { ObjectId } = Types;
import {
  Usuarios,
  Profesional,
} from "../../src/database";


onesignalRouter.get("/save-userid-notification", (req, res) => {
    const { UserId, id } = req.query;
    if (UserId && id) {
      Usuarios.findOneAndUpdate(
        { _id: ObjectId(id) },
        { $set: { UserID: UserId } },
        (err, user) => {}
      ).catch((err) => {
        console.log(err);
      });
    }
  });
  
  onesignalRouter.get("/save-profesionalid-notification", (req, res) => {
    const { UserId, id } = req.query;
    if (UserId && id) {
      Profesional.findOneAndUpdate(
        { _id: ObjectId(id) },
        { $set: { UserID: UserId } },
        (err, user) => {}
      ).catch((err) => {
        console.log(err);
      });
    }
  });
  
  onesignalRouter.get("/send-push-notification", (req, res) => {
    const { IdOnesignal, textmessage } = req.query;
    var sendNotification = function (data) {
      var headers = {
        "Content-Type": "application/json; charset=utf-8",
      };
      var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers,
      };
      var https = require("https");
      var req = https.request(options, function (res) {
        res.on("data", function (data) {
          console.log("Response:");
          console.log(JSON.parse(data));
        });
      });
  
      req.on("error", function (e) {
        console.log("ERROR:");
        console.log(e);
      });
  
      req.write(JSON.stringify(data));
      req.end();
    };
  
    var message = {
      app_id: "e461eb6c-b20a-4a47-8923-ff44868ed8b2",
      contents: { en: `"${textmessage}"` },
      ios_badgeCount: 1,
      ios_badgeType: "Increase",
      include_player_ids: [`${IdOnesignal}`],
    };
    sendNotification(message);
  });
  
  onesignalRouter.get("/send-push-notification-profesional", (req, res) => {
    const { IdOnesignal, textmessage } = req.query;
    var sendNotification = function (data) {
      var headers = {
        "Content-Type": "application/json; charset=utf-8",
      };
      var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers,
      };
      var https = require("https");
      var req = https.request(options, function (res) {
        res.on("data", function (data) {
          console.log("Response:");
        });
      });
  
      req.on("error", function (e) {
        console.log("ERROR:");
        console.log(e);
      });
  
      req.write(JSON.stringify(data));
      req.end();
    };
  
    var message = {
      app_id: "3b850d4c-457b-4e1b-bc7d-e5e4dab86b27",
      contents: { en: `"${textmessage}"` },
      ios_badgeCount: 1,
      ios_badgeType: "Increase",
      include_player_ids: [`${IdOnesignal}`],
    };
    sendNotification(message);
  });

  onesignalRouter.get("/send-push-notification-message", (req, res) => {
    const { IdOnesignal, messages, name } = req.query;
    var sendNotification = function (data) {
      var headers = {
        "Content-Type": "application/json; charset=utf-8",
      };
      var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers,
      };
      var https = require("https");
      var req = https.request(options, function (res) {
        res.on("data", function (data) {
          console.log("Response:");
          console.log(JSON.parse(data));
        });
      });
  
      req.on("error", function (e) {
        console.log("ERROR:");
        console.log(e);
      });
  
      req.write(JSON.stringify(data));
      req.end();
    };
  
    var message = {
      app_id: "e461eb6c-b20a-4a47-8923-ff44868ed8b2",
      headings: { en: name },
      contents: { en: messages },
      ios_badgeCount: 1,
      ios_badgeType: "Increase",
      include_player_ids: [`${IdOnesignal}`],
    };
    sendNotification(message);
  });


  onesignalRouter.get("/send-push-notification-message-profesional", (req, res) => {
    const { IdOnesignal, messages, name } = req.query;
    var sendNotification = function (data) {
      var headers = {
        "Content-Type": "application/json; charset=utf-8",
      };
      var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers,
      };
      var https = require("https");
      var req = https.request(options, function (res) {
        res.on("data", function (data) {
          console.log("Response:");
        });
      });
  
      req.on("error", function (e) {
        console.log("ERROR:");
        console.log(e);
      });
  
      req.write(JSON.stringify(data));
      req.end();
    };
  
    var message = {
      app_id: "3b850d4c-457b-4e1b-bc7d-e5e4dab86b27",
      headings: { en: name },
      contents: { en: messages },
      ios_badgeCount: 1,
      ios_badgeType: "Increase",
      include_player_ids: [`${IdOnesignal}`],
    };
    sendNotification(message);
  });

  module.exports = onesignalRouter