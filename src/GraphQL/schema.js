const { gql } = require("apollo-server-express");

export const typeDefs = gql`
  scalar Date

  type Conversationgeneralresponse {
    success: Boolean!
    message: String!
    conversation: [ConversationByClient]
  }

  type getStatisticsResponse {
    success: Boolean!
    message: String
    data: [Statistics]
  }

  type GenerarResponseUSerPlus {
    success: Boolean!
    message: String
    data: UsuarioPlus
  }

  type AlluserResponse {
    success: Boolean!
    message: String
    data: [Usuario]
  }

  type ProfVisitasResponse {
    success: Boolean!
    message: String
    list: [ProfVisitas]
  }

  type MypacienteResponse {
    success: Boolean!
    message: String
    list: [Usuario]
  }

  type ProfVisitas {
    id: ID
    profesional_id: String
    mes_id: Int
    ano: Int
    visitas: Int
  }

  type Statistics {
    name: String
    Ene: Int
    Feb: Int
    Mar: Int
    Abr: Int
    May: Int
    Jun: Int
    Jul: Int
    Aug: Int
    Sep: Int
    Oct: Int
    Nov: Int
    Dic: Int
  }

  type messagenoReadResponse {
    success: Boolean!
    message: String
    conversation: [MessageNoRead]
  }

  type MessageNoRead {
    _id: String
    count: Int
  }

  type PagoResponse {
    success: Boolean!
    message: String
    data: Pago
  }

  type PagoResponseList {
    success: Boolean!
    message: String
    data: Pago
  }

  type GeneralResponseProOne {
    success: Boolean
    message: String
    data: Profesional
  }

  type AutenticarUsuarioResponse {
    success: Boolean!
    message: String
    data: AutenticarUsuario
  }

  type ProfessionalListRating {
    success: Boolean!
    message: String
    list: [ProfessionalRating]
  }

  type getNotificationResponse {
    success: Boolean!
    message: String
    notifications: [Notification]
  }

  type AutenticarProfesionalResponse {
    success: Boolean!
    message: String
    data: AutenticarProfesional
  }

  type AutenticarProtectoraResponse {
    success: Boolean!
    message: String
    data: AutenticarProtectora
  }

  type AutenticarAdminResponse {
    success: Boolean!
    message: String
    data: AutenticarAdmin
  }

  type CategoryResponse {
    success: Boolean!
    message: String
    data: Category
  }

  type UsuarioFavoritoProResponse {
    success: Boolean!
    message: String
    list: [UsuarioFavoritoPro]
  }

  type UsuarioFavoritoPro {
    id: ID
    profesionalId: ID
    usuarioId: ID
    profesional: Profesional
  }

  type ProfesionalCityResponse {
    success: Boolean!
    message: String
    data: [Profesional]
  }

  type ClienteResponse {
    success: Boolean!
    message: String
    data: [Usuario]
  }

  type ConsultaListResponse {
    success: Boolean!
    message: String
    list: [Consultas]
  }

  type ConsultaResponse {
    success: Boolean!
    message: String
    data: Consultas
  }

  type ConsultaListAdminResponse {
    success: Boolean!
    message: String
    list: [Consultas]
  }

  type ConsultaPaymentResponse {
    success: Boolean!
    message: String
    list: Consultas
  }

  type AutenticarUsuario {
    token: String!
    id: String!
    verifyPhone: Boolean!
  }

  type AutenticarProfesional {
    token: String!
    id: String!
    verifyPhone: Boolean!
  }

  type AutenticarProtectora {
    token: String!
    id: String!
  }

  type Conversation {
    _id: String
    prof: ID
    user: ID
    profesional: Profesional
    usuario: Usuario
    messagechat: [ConversationList]
  }

  type ConversationList {
    _id: String
    text: String
    image: String
    read: Boolean
    createdAt: String
    user: Userchat
    sent: Boolean
    received: Boolean
    pending: Boolean
  }

  type Userchat {
    _id: String
    name: String
    avatar: String
  }

  type AutenticarAdmin {
    token: String!
    id: String!
  }

  type GeneralResponse {
    success: Boolean!
    message: String
  }

  type MascotaListResponse {
    success: Boolean!
    message: String
    list: [Mascota]
  }

  type TeamResponse {
    success: Boolean!
    message: String
    data: UserAdmin
  }

  type TeamResponsesearch {
    success: Boolean!
    message: String
    data: [UserAdmin]
  }

  type MascotaResponse {
    success: Boolean!
    message: String
    data: Mascota
  }

  type DiagnosticoResponse {
    success: Boolean!
    message: String
    list: [Diagnostico]
  }

  type DepositoCreateResponse {
    success: Boolean!
    message: String
    data: Deposito
  }

  type DiagnosticoResponsecreate {
    success: Boolean!
    message: String
    data: Diagnostico
  }

  type Diagnostico {
    _id: ID
    profesional: Profesionaldiag
    diagnostico: String
    mascota: String
    created_at: String
  }

  type DepositoResponselist {
    success: Boolean!
    message: String
    list: [Deposito]
  }

  type Deposito {
    id: ID
    fecha: String
    estado: String
    total: String
    profesionalID: String
    created_at: String
  }

  type Profesionaldiag {
    nombre: String
    apellidos: String
    profesion: String
    avatar: String
  }

  type CrearUsuarioResponse {
    success: Boolean!
    message: String
    data: Usuario
  }

  type CrearProfesionalResponse {
    success: Boolean!
    message: String
    data: Profesional
  }

  type CrearadminResponse {
    success: Boolean!
    message: String
    data: UserAdmin
  }

  type AdoptarResponse {
    success: Boolean!
    message: String
    data: [Adoptar]
  }

  type Adoptar {
    id: String
    name: String
    lastName: String
    email: String
    city: String
    phone: String
    experiencia: String
    otra: String
    fecha: Date
    nino: Boolean
    mascota: String
    macotas: Mascota
    usuario: String
    protectora: String
    estado: String
    razon: String
    created_at: Date
    UserID: String
  }

  type ProfessionalRating {
    id: ID
    coment: String
    updated_at: Date
    rate: Int
    customer: Usuario
  }

  type Usuario {
    id: ID
    email: String
    UserID: String
    nombre: String
    apellidos: String
    ciudad: String
    telefono: String
    avatar: String
    StripeID: String
    MyVet: ID
    connected: Boolean
    isNotVerified: Boolean
    lastTime: String
    verifyPhone: Boolean
    setVideoConsultas: Int
    isPlus: Boolean
    notificacion: Boolean
    created_at: String
    suscription: suscripcionobjet
  }

  type suscripcionobjet {
    id: String
    current_period_end: String
    current_period_start: String
    trial_end: String
    trial_start: String
    status: String
    livemode: Boolean
  }

  type Pago {
    id: ID
    nombre: String
    iban: String
    profesionalID: String
  }

  type Profesional {
    id: ID
    email: String
    UserID: String
    nombre: String
    apellidos: String
    ciudad: String
    telefono: String
    avatar: String
    profesion: String
    experiencia: String
    descricion: String
    Precio: String
    connected: Boolean
    lastTime: String
    verifyPhone: Boolean
    anadidoFavorito: Boolean
    notificacion: Boolean
    isProfesional: Boolean
    isVerified: Boolean
    isAvailable: Boolean
    isPlusVet: Boolean
    category_id: String
    visitas: Int
    professionalRatinglist: [ProfessionalRating]
    consultas: [Consultas]
    created_at: String
  }

  type UserAdmin {
    id: ID
    email: String
    UserID: String
    nombre: String
    apellidos: String
    ciudad: String
    telefono: String
    avatar: String
    cargo: String
    descricion: String
    twitter: String
    linkedin: String
    notificacion: Boolean
    isAdmin: Boolean
    isAvailable: Boolean
    createBy: String
    created_at: String
  }

  type Notification {
    _id: String
    user: ID
    usuario: Usuario
    profesional: Profesional
    consulta: Consultas
    read: Boolean
    type: String
    createdAt: String
  }

  type File {
    filename: String
    mimetype: String
    encoding: String
  }

  type Mascota {
    id: ID
    name: String
    age: String
    avatar: String
    usuario: ID
    especie: String
    raza: String
    genero: String
    peso: String
    microchip: String
    vacunas: String
    alergias: String
    castrado: Boolean
    consutas: ConsutasMascota
    centro: Protectora
    protectoraID: String
    protectora: Boolean
    ciudad: String
    descripcion: String
  }

  type ConsutasMascota {
    profesional: ID
    nota: String
    createdAt: String
  }

  type Consulta {
    id: ID
    cupon: ID
    nota: String
    aceptaTerminos: Boolean
    endDate: String
    time: String
    cantidad: Int
    usuario: ID
    profesional: ID
    estado: String
    status: String
    progreso: String
    created_at: String
    mascota: ID
  }

  type Category {
    id: ID
    title: String
    image: String
    font: String
    description: String
    img: String
    profesionales: [Profesional]
  }

  type Cupon {
    id: ID
    clave: String
    descuento: Float
    tipo: String
  }

  type UsuarioPlus {
    usuario: Usuario
    profesional: Profesional
  }

  type FullConsulta {
    id: ID
    cupon: Cupon
    nota: String
    aceptaTerminos: Boolean
    cantidad: Int
    usuario: Usuario
    profesional: Usuario
  }

  type ConversationByClient {
    _id: String
    prof: ID
    user: ID
    profesional: Profesional
    usuario: Usuario
    messagechat: [ConversationList]
  }

  type Consultas {
    id: ID
    cupon: ID
    nota: String
    aceptaTerminos: Boolean
    endDate: String
    time: String
    cantidad: Int
    usuario: Usuario
    descuento: Cupon
    profesional: Profesional
    estado: String
    status: String
    progreso: String
    created_at: String
    mascota: Mascota
    ConsultaRating: [ProfessionalRating]
  }

  type GetadreesProtectora {
    calle: String
    cp: String
    numero: String
    ciudad: String
    provincia: String
    lat: String
    lgn: String
  }

  type Protectora {
    id: String
    name: String
    email: String
    logo: String
    adrees: GetadreesProtectora
    phone: String
    password: String
    descripcion: String
    active: Boolean
    web: String
  }

  input NotificationInput {
    user: String
    usuario: String
    profesional: String
    consulta: String
    read: Boolean
    type: String
  }

  input UsuarioInput {
    nombre: String!
    apellidos: String!
    email: String!
    password: String!
  }

  input ProfesionalInput {
    nombre: String!
    apellidos: String!
    email: String!
    password: String!
  }

  input userAdmininput {
    nombre: String!
    apellidos: String!
    email: String!
    createBy: String!
    password: String!
  }

  input FileInput {
    filename: String
    mimetype: String
    encoding: String
  }

  input DepositoInput {
    fecha: String
    estado: String
    total: String
    profesionalID: String
  }

  input Mascotainput {
    name: String
    age: String
    avatar: String
    usuario: ID
    especie: String
    raza: String
    genero: String
    peso: String
    microchip: String
    vacunas: String
    alergias: String
    castrado: Boolean
    protectoraID: String
    protectora: Boolean
    ciudad: String
    descripcion: String
  }

  input ActualizarMascotainput {
    id: String
    name: String
    age: String
    avatar: String
    usuario: ID
    especie: String
    raza: String
    genero: String
    peso: String
    microchip: String
    vacunas: String
    alergias: String
    castrado: Boolean
    protectoraID: String
    protectora: Boolean
    ciudad: String
    descripcion: String
  }

  input Diagnosticoinput {
    profesional: ProfesiDiag
    diagnostico: String
    mascota: ID
  }

  input ProfesiDiag {
    nombre: String
    apellidos: String
    profesion: String
    avatar: String
  }

  input CategoryInput {
    title: String!
    image: String
    description: String
    font: String
  }

  input ActualizarUsuarioInput {
    id: ID!
    email: String
    nombre: String
    apellidos: String
    ciudad: String
    telefono: String
    avatar: String
    MyVet: ID
    setVideoConsultas: Int
    isPlus: Boolean
    notificacion: Boolean
  }

  input ActualizarProfesionalInput {
    id: ID!
    email: String
    category_id: String
    nombre: String
    apellidos: String
    telefono: String
    ciudad: String
    notificacion: Boolean
    isAvailable: Boolean
    avatar: String
    profesion: String
    experiencia: String
    descricion: String
    Precio: String
    isVerified: Boolean
  }

  input ActualizaradminInput {
    id: ID!
    email: String
    nombre: String
    apellidos: String
    ciudad: String
    telefono: String
    avatar: String
    notificacion: Boolean
    cargo: String
    twitter: String
    linkedin: String
    descricion: String
    createBy: String
  }

  input DateRangeInput {
    fromDate: String
    toDate: String
  }

  input PagoInput {
    nombre: String
    iban: String
    profesionalID: String
  }

  input ConsultaCreationInput {
    endDate: String
    time: String
    clave: String
    nota: String
    id: ID
    aceptaTerminos: Boolean
    cupon: ID
    profesional: ID
    mascota: ID
    cantidad: Int
    usuario: ID
  }

  input CuponCreationInput {
    clave: String!
    descuento: Int
    tipo: String
  }

  input AdoptarInput {
    name: String
    lastName: String
    email: String
    city: String
    phone: String
    experiencia: String
    otra: String
    fecha: Date
    nino: Boolean
    mascota: String
    usuario: String
    estado: String
    protectora: String
    UserID: String
  }

  input AdoptarActualizarInput {
    id: String
    name: String
    lastName: String
    email: String
    city: String
    phone: String
    experiencia: String
    otra: String
    fecha: Date
    nino: Boolean
    mascota: String
    usuario: String
    protectora: String
    estado: String
    razon: String
    UserID: String
  }

  input adreesProtectora {
    calle: String
    cp: String
    numero: String
    ciudad: String
    provincia: String
    lat: String
    lgn: String
  }

  input ProtectoraInputActualizar {
    id: String
    name: String
    email: String
    logo: String
    adrees: adreesProtectora
    phone: String
    password: String
    descripcion: String
    active: Boolean
    web: String
  }

  input ProtectoraInput {
    name: String
    email: String
    password: String
    phone: String
  }

  type Query {
    getUsuario(id: ID): Usuario
    getAlluser: AlluserResponse
    uploads: [File]
    getMascota(usuarios: ID!): MascotaListResponse
    getMascotaprotectora(id: ID): MascotaListResponse
    getMascotaforAdopcionClients(
      especie: String
      ciudad: String
      genero: String
    ): MascotaListResponse
    getMascotaadmin(id: ID!): MascotaListResponse
    getMascotaForAdopcion(protectoraID: ID!): MascotaListResponse
    getMascotaAdopciones: MascotaListResponse
    getDiagnostico(id: ID!): DiagnosticoResponse
    getProfessionalRating(id: ID!): ProfessionalListRating
    getProfesional(id: ID): Profesional
    getProfesionalall: [Profesional]
    getProfesionaforSuscribcion: [Profesional]
    getProfesionalone(id: ID!, updateProfVisit: Boolean): GeneralResponseProOne
    getProfesionalCity(city: String): ProfesionalCityResponse

    getmyPacientes(id: ID, search: String): MypacienteResponse

    getUserPlus(id: ID!): GenerarResponseUSerPlus

    getProfesionalSearch(search: String): ProfesionalCityResponse

    getUsuarioFavoritoPro(id: ID!): UsuarioFavoritoProResponse
    getUsuarioFavoritoall(id: ID!): UsuarioFavoritoProResponse

    getTeam(id: ID!): TeamResponse
    getTeamSearch: TeamResponsesearch

    getProfesionalSearchadmin(search: String): ProfesionalCityResponse

    getClientesSearchadmin(search: String): ClienteResponse

    getConsultaByProfessional(
      profesional: ID
      dateRange: DateRangeInput
    ): ConsultaListResponse

    getConsultaadmin(
      search: String
      dateRange: DateRangeInput
    ): ConsultaListAdminResponse

    getConsultaByUsuario(
      usuarios: ID
      dateRange: DateRangeInput
    ): ConsultaListResponse
    getConsultaByPayment(id: ID!): ConsultaPaymentResponse

    getCupon(clave: String): Cupon

    getAdmin(id: ID): UserAdmin

    obtenerUsuario: Usuario

    obtenerAdmin: UserAdmin

    obtenerProfesinal: Profesional

    getCuponAll: Cupon

    getFullConsulta(id: ID): FullConsulta

    getConsulta(id: ID): Consultas

    getAllCupon: [Cupon]

    getNotifications(Id: ID!): getNotificationResponse

    getDeposito(
      page: Int
      dateRange: DateRangeInput
      id: ID!
    ): DepositoResponselist

    getPago(id: ID!): PagoResponseList

    getCategories: [Category]

    getCategory(id: ID, price: Int, city: String): CategoryResponse

    getConversationclient(userId: ID): Conversationgeneralresponse
    getConversationProf(ProfId: ID): Conversationgeneralresponse
    getMessageNoReadClient(
      conversationID: ID!
      profId: ID!
    ): messagenoReadResponse

    getMessageNoReadProf(
      conversationID: ID!
      clientId: ID!
    ): messagenoReadResponse

    getStatistics(userId: ID): getStatisticsResponse
    getProfVisitas(profesionalId: ID): ProfVisitasResponse

    getProtectoras(id: ID): Protectora
    getAdopcionClient(usuario: ID): AdoptarResponse
    getAdopcionProtectora(protectora: ID estado: String): AdoptarResponse
  }

  type Mutation {
    singleUpload(file: Upload): File
    crearUsuario(input: UsuarioInput): CrearUsuarioResponse
    actualizarUsuario(input: ActualizarUsuarioInput): Usuario
    eliminarUsuario(id: ID!): GeneralResponse
    crearModificarConsulta(input: ConsultaCreationInput): ConsultaResponse
    crearMascota(input: Mascotainput): MascotaResponse
    crearDiagnostico(input: Diagnosticoinput): DiagnosticoResponsecreate
    eliminarMascota(id: ID!): GeneralResponse
    eliminarconsulta(id: ID!): GeneralResponse

    readMessage(conversationID: ID!): GeneralResponse
    recibeMessage(conversationID: ID!): GeneralResponse
    deleteConversation(id: ID!): GeneralResponse

    crearCategory(input: CategoryInput): Category
    eliminarCategory(id: ID!): String

    crearUsuarioFavoritoPro(profesionalId: ID!, usuarioId: ID!): GeneralResponse
    eliminarUsuarioFavoritoPro(id: ID!): GeneralResponse

    autenticarUsuario(
      email: String!
      password: String!
    ): AutenticarUsuarioResponse

    crearCupon(input: CuponCreationInput): Cupon

    #/////////////admin///////////
    crearadmin(input: userAdmininput): CrearadminResponse
    autenticaradmin(email: String!, password: String!): AutenticarAdminResponse

    actualizaradmin(input: ActualizaradminInput): UserAdmin

    eliminarAdmin(id: ID!): GeneralResponse

    eliminarCupon(id: ID!): GeneralResponse

    #////////////profesional///////////

    crearProfesional(input: ProfesionalInput): CrearProfesionalResponse
    actualizarProfesional(input: ActualizarProfesionalInput): Profesional
    eliminarProfesional(id: ID!): GeneralResponse
    autenticarProfesional(
      email: String!
      password: String!
    ): AutenticarProfesionalResponse

    #///////////// end ///////////////////

    consultaProceed(
      consultaID: ID!
      estado: String!
      progreso: String!
      status: String!
      nota: String
      coment: String
      rate: Int
      descripcionproblem: String
    ): GeneralResponse

    readNotification(notificationId: ID!): GeneralResponse
    createNotification(input: NotificationInput): GeneralResponse

    crearDeposito(input: DepositoInput): DepositoCreateResponse
    eliminarDeposito(id: ID!): GeneralResponse

    crearPago(input: PagoInput): PagoResponse
    eliminarPago(id: ID!): GeneralResponse

    actualizarMascota(input: ActualizarMascotainput): GeneralResponse

    crearProtectora(input: ProtectoraInput): GeneralResponse
    autenticarProtectora(
      email: String!
      password: String!
    ): AutenticarProtectoraResponse
    eliminarProtectora(id: ID): GeneralResponse
    actualizarProtectora(input: ProtectoraInputActualizar): GeneralResponse

    crearSolititud(input: AdoptarInput): GeneralResponse
    actualizarsolicitud(input: AdoptarActualizarInput): GeneralResponse
    eliminarSolicitud(id: ID): GeneralResponse
  }
`;
