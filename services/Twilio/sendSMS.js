const express = require("express");
const twilioRauter = express.Router();
import { Types } from "mongoose";
const { ObjectId } = Types;
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
import { Usuarios, Profesional } from "../../src/database";
var cors = require("cors");
const whitelist = [
  "https://vetec.es",
  "https://www.vetec.es",
  "https://profesionales.vetec.es",
  "https://dashboard.vetec.es",
];

const corsOptions = {
  origin: (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      //callback(new Error('Not allowed by CORS'));
      callback(null, true);
    }
  },
  optionsSuccessStatus: 200,
};

const accountSid = process.env.accountSid;
const authToken = process.env.authToken;
const SERVICESID = process.env.SERVICESID;
const client = require("twilio")(accountSid, authToken, SERVICESID);

twilioRauter.get("/send-message-whatsapp", cors(corsOptions), (req, res) => {
  const { recipient, textmessage } = req.query;
  client.messages
    .create({
      body: textmessage,
      from: "whatsapp:+14155238886",
      to: `whatsapp:+${recipient}`,
    })
    .then((message) => {
      console.log("done >>>>>>>", message.sid);
      res.status(200).send({ success: true, data: message });
    })
    .catch((err) => {
      console.log("Err sen message", err);
      res.status(404).send({ success: false, data: err });
    });
});

twilioRauter.get("/send-message", cors(corsOptions), (req, res) => {
  const { recipient, textmessage } = req.query;
  client.messages
    .create({
      body: textmessage,
      from: "+16368218171",
      to: `+${recipient}`,
    })
    .then((message) => console.log("done >>>>>>>", message.sid))
    .catch((err) => console.log("Err sen message", err));
});

twilioRauter.get("/send-message-dowload", cors(corsOptions), (req, res) => {
  const { recipient, textmessage } = req.query;
  client.messages
    .create({
      body: textmessage,
      from: "+16368218171",
      to: `+${recipient}`,
    })
    .then((message) => {
      console.log("done >>>>>>>", message.sid);
      res.status(200).send({ success: true, data: message });
    })
    .catch((err) => {
      console.log("Err sen message", err);
      res.status(404).send({ success: false, data: err });
    });
});

twilioRauter.get("/verify-phone", cors(corsOptions), async (req, res) => {
  const { phone } = req.query;
  const channel = "sms";
  const verificationRequest = await client.verify
    .services(SERVICESID)
    .verifications.create({ to: `+${phone}`, channel })
    .then((message) => {
      console.log("done >>>>>>>", message);
      res.status(200).send({ success: true, data: verificationRequest });
    })
    .catch((err) => {
      console.log("Err sen message", err);
      res.status(404).send({ success: false, data: err });
    });
});

twilioRauter.get("/verify-code", cors(corsOptions), async (req, res) => {
  const { phone, code, id } = req.query;
  console.log(phone, code, id);
  client.verify
    .services(SERVICESID)
    .verificationChecks.create({ to: `+${phone}`, code: code })
    .then((verification_check) => {
      res.status(200).send({ success: true, data: verification_check.status });
      if (verification_check.status === "approved") {
        Usuarios.findOneAndUpdate(
          { _id: ObjectId(id) },
          {
            $set: { verifyPhone: true, telefono: phone },
          },
          (err, user) => {
            if (err) {
              console.log(err);
            } else {
              console.log(user);
            }
          }
        );
      } else {
        res
          .status(400)
          .send({ success: false, data: verification_check.status });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(404).send({ success: false, data: err });
    });
});

twilioRauter.get(
  "/verify-code-profesional",
  cors(corsOptions),
  async (req, res) => {
    const { phone, code, id } = req.query;
    console.log(phone, code, id);
    client.verify
      .services(SERVICESID)
      .verificationChecks.create({ to: `+${phone}`, code: code })
      .then((verification_check) => {
        res
          .status(200)
          .send({ success: true, data: verification_check.status });
        if (verification_check.status === "approved") {
          Profesional.findOneAndUpdate(
            { _id: ObjectId(id) },
            {
              $set: { verifyPhone: true, telefono: phone },
            },
            (err, user) => {
              if (err) {
                console.log(err);
              } else {
                console.log("actualizados >>", user);
              }
            }
          );
        } else {
          res
            .status(400)
            .send({ success: false, data: verification_check.status });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(404).send({ success: false, data: err });
      });
  }
);

module.exports = twilioRauter;
