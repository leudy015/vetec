var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const usuarioFavoritoProSchema = new mongoose.Schema(
    {
      usuarioId: String,
      profesionalId: String,
    },
    { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
  );
  
const UsuarioFavoritoPro = mongoose.model(
    "usuarioFavoritoProducto",
    usuarioFavoritoProSchema
  );

  module.exports = UsuarioFavoritoPro;