const express = require("express");
const mailRouter = express.Router();
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
var cors = require("cors");
const mailjet = require ('node-mailjet').connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE)

const whitelist = [
    "https://vetec.es",
    "https://www.vetec.es",
    "https://profesionales.vetec.es",
    "https://dashboard.vetec.es",
  ];
  
  const corsOptions = {
    origin: (origin, callback) => {
      if (whitelist.indexOf(origin) !== -1) {
        callback(null, true);
      } else {
        //callback(new Error('Not allowed by CORS'));
        callback(null, true);
      }
    },
    optionsSuccessStatus: 200,
  };

  mailRouter.get("/save-email", cors(corsOptions), async (req, res) => {
    const { email, name } = req.query;
    const request = mailjet
    .post("contact")
      .request({
          "Email": email,
          "IsExcludedFromCampaigns":"false",
        "Name": name,
      })
  request
      .then((result) => {
      console.log(result.body.Data[0].ID)
      const request = mailjet
        .post("contact")
        .id(result.body.Data[0].ID)
        .action("managecontactslists")
        .request({
        "ContactsLists": [{
                "ListID": 2442594,
                "Action": "addnoforce"
            }]
        })
        request
        .then((result) => {
          console.log(result.body.Data[0])
          res.json(result.body)
        })
        .catch((err) => {
            console.log(err.statusCode)
        })

      })
      .catch((err) => {
          console.log(err.statusCode)
      })
  });

  module.exports = mailRouter;