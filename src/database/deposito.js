var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const depositoSchema = new mongoose.Schema(
  {
    fecha: Date,
    estado: String,
    total: String,
    profesionalID: String,
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);
const Deposito = mongoose.model("deposito", depositoSchema);

module.exports = Deposito;
