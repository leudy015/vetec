var bcrypt = require("bcryptjs");
var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const profesionalSchema = new mongoose.Schema(
  {
    nombre: { type: String, text: true },
    apellidos: { type: String, text: true },
    email: String,
    ciudad: String,
    telefono: String,
    avatar: String,
    password: String,
    UserID: String,
    profesion: String,
    experiencia: String,
    descricion: String,
    Precio: String,
    connected: { type: mongoose.Schema.Types.Boolean, default: false },
    verifyPhone: { type: mongoose.Schema.Types.Boolean, default: false },
    lastTime: Date,
    isPlusVet: { type: mongoose.Schema.Types.Boolean, default: false },
    category_id: { type: mongoose.Schema.Types.ObjectId, ref: "categories" },
    notificacion: { type: mongoose.Schema.Types.Boolean, default: false },
    isProfesional: { type: mongoose.Schema.Types.Boolean, default: true },
    isVerified: { type: mongoose.Schema.Types.Boolean, default: false },
    isAvailable: { type: mongoose.Schema.Types.Boolean, default: false },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

// hashear los password antes de guardar
profesionalSchema.pre("save", function (next) {
  // Si el password no esta hasheado...
  if (!this.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(this.password, salt, (err, hash) => {
      if (err) return next(err);
      this.password = hash;
      next();
    });
  });
});

const Profesional = mongoose.model("profesionals", profesionalSchema);

module.exports = Profesional;
  