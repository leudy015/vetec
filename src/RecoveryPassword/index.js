const express = require("express");
const recoveryRouter = express.Router();
var bcrypt = require("bcryptjs");
import crypto from "crypto";
const nodemailer = require("nodemailer");
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
import { Usuarios, Profesional, Useradmin, Protectora } from "./../database";
import recoverPasswordEmail from "../EmailSistems/email";
import recoverPasswordEmailpro from "../EmailSistems/emailprofesional";
import recoverPasswordEmailTeam from "../EmailSistems/emailteam";
import recoverPasswordEmailProtec from "../EmailSistems/emailprotectora";
var cors = require("cors");

const whitelist = [
  "https://vetec.es",
  "https://www.vetec.es",
  "https://profesionales.vetec.es",
  "https://dashboard.vetec.es",
];

const corsOptions = {
  origin: (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      //callback(new Error('Not allowed by CORS'));
      callback(null, true);
    }
  },
  optionsSuccessStatus: 200,
};

recoveryRouter.get("/forgotpassword", cors(corsOptions), (req, res) => {
  const email = req.query.email;
  Usuarios.find({ email: email }, (err, data) => {
    if (data.length < 1 || err) {
      res.send({
        noEmail: true,
        message: "Aún no tenemos este correo eletrónico",
      });
      res
        .status(404)
        .json({ message: "Aún no tenemos este correo eletrónico" });
    } else {
      const token = crypto.randomBytes(20).toString("hex");

      // write to database

      const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        service: "gmail",
        port: 465,
        secure: true,
        auth: {
          user: process.env.EMAIL_ADDRESS,
          pass: `${process.env.EMAIL_PASSWORD}`,
        },
      });
      const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: email,
        subject: "Recuperar contraseña Vetec App",
        text: "Recuperar contraseña Vetec App",
        html: recoverPasswordEmail(token),
      };
      transporter.sendMail(mailOptions, (err, response) => {
        if (err) {
          console.log("err:", err);
        } else {
          var newvalues = { $set: { forgotPasswordToken: token } };

          Usuarios.findOneAndUpdate(
            { email: email },
            newvalues,
            {
              //options
              returnNewDocument: true,
              new: true,
              strict: false,
              useFindAndModify: false,
            },
            (err, updated) => {
              console.log(updated);
              res.status(200).json({ message: "Email Sent" });
            }
          );
        }
      });
    }
  });
});

recoveryRouter.get(
  "/forgotpassword-profesional",
  cors(corsOptions),
  (req, res) => {
    const email = req.query.email;
    Profesional.find({ email: email }, (err, data) => {
      if (data.length < 1 || err) {
        res.send({
          noEmail: true,
          message: "Aún no tenemos este correo eletrónico",
        });
      } else {
        const token = crypto.randomBytes(20).toString("hex");

        // write to database

        const transporter = nodemailer.createTransport({
          host: "smtp.gmail.com",
          service: "gmail",
          port: 465,
          secure: true,
          auth: {
            user: process.env.EMAIL_ADDRESS,
            pass: `${process.env.EMAIL_PASSWORD}`,
          },
        });
        const mailOptions = {
          from: process.env.EMAIL_ADDRESS,
          to: email,
          subject: "Recuperar contraseña Vetec App PRO",
          text: "Recuperar contraseña Vetec App PRO",
          html: recoverPasswordEmailpro(token),
        };
        transporter.sendMail(mailOptions, (err, response) => {
          if (err) {
            console.log("err:", err);
          } else {
            var newvalues = { $set: { forgotPasswordToken: token } };

            Profesional.findOneAndUpdate(
              { email: email },
              newvalues,
              {
                //options
                returnNewDocument: true,
                new: true,
                strict: false,
                useFindAndModify: false,
              },
              (err, updated) => {
                console.log(updated);
                res.status(200).json({ message: "Email Sent" });
              }
            );
          }
        });
      }
    });
  }
);

recoveryRouter.get("/forgotpassword-team", cors(corsOptions), (req, res) => {
  const email = req.query.email;
  Useradmin.find({ email: email }, (err, data) => {
    if (data.length < 1 || err) {
      res.send({
        noEmail: true,
        message: "Aún no tenemos este correo eletrónico",
      });
    } else {
      const token = crypto.randomBytes(20).toString("hex");

      // write to database

      const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        service: "gmail",
        port: 465,
        secure: true,
        auth: {
          user: process.env.EMAIL_ADDRESS,
          pass: `${process.env.EMAIL_PASSWORD}`,
        },
      });
      const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: email,
        subject: "Recuperar contraseña Vetec for Team",
        text: "Recuperar contraseña  Vetec for Team",
        html: recoverPasswordEmailTeam(token),
      };
      transporter.sendMail(mailOptions, (err, response) => {
        if (err) {
          console.log("err:", err);
        } else {
          var newvalues = { $set: { forgotPasswordToken: token } };

          Useradmin.findOneAndUpdate(
            { email: email },
            newvalues,
            {
              //options
              returnNewDocument: true,
              new: true,
              strict: false,
              useFindAndModify: false,
            },
            (err, updated) => {
              console.log(updated);
              res.status(200).json({ message: "Email Sent" });
            }
          );
        }
      });
    }
  });
});

recoveryRouter.post("/tokenValidation-team", (req, res) => {
  Useradmin.find({ forgotPasswordToken: req.body.token }, (err, data) => {
    if (err || data.length < 1) {
      if (err) console.log(err);
      res.status(200).json({ isValid: false, email: "" });
    } else if (data.length > 0) {
      res.status(200).json({ isValid: true, email: data[0].email });
    }
  });
});

recoveryRouter.post("/resetPassword-team", cors(corsOptions), (req, res) => {
  var newvalues = { $unset: { forgotPasswordToken: req.body.token } };

  Useradmin.findOneAndUpdate(
    { email: req.body.email },
    newvalues,
    {
      //options
      returnNewDocument: true,
      new: true,
      strict: false,
      useFindAndModify: false,
    },
    (err, updated) => {
      if (err) console.log(err);
      bcrypt.genSalt(10, (err, salt) => {
        if (err) console.log(err);

        bcrypt.hash(req.body.password, salt, (err, hash) => {
          if (err) console.log(err);
          var newvalues2 = { $set: { password: hash } };
          Useradmin.findOneAndUpdate(
            { email: req.body.email },
            newvalues2,
            {
              //options
              returnNewDocument: true,
              new: true,
              strict: false,
              useFindAndModify: false,
            },
            (err, updated) => {
              if (err) console.log(err);

              console.log(updated);
              res.status(200).json({ changed: true });
            }
          );
        });
      });
    }
  );
});

/* recuperar contraseña protectora */

recoveryRouter.get(
  "/forgotpassword-protectora",
  cors(corsOptions),
  (req, res) => {
    const email = req.query.email;
    Protectora.find({ email: email }, (err, data) => {
      if (data.length < 1 || err) {
        res.send({
          noEmail: true,
          message: "Aún no tenemos este correo eletrónico",
        });
      } else {
        const token = crypto.randomBytes(20).toString("hex");

        // write to database

        const transporter = nodemailer.createTransport({
          host: "smtp.gmail.com",
          service: "gmail",
          port: 465,
          secure: true,
          auth: {
            user: process.env.EMAIL_ADDRESS,
            pass: `${process.env.EMAIL_PASSWORD}`,
          },
        });
        const mailOptions = {
          from: process.env.EMAIL_ADDRESS,
          to: email,
          subject: "Recuperar contraseña Vetec para protectora",
          text: "Recuperar contraseña Vetec para protectora",
          html: recoverPasswordEmailProtec(token),
        };
        transporter.sendMail(mailOptions, (err, response) => {
          if (err) {
            console.log("err:", err);
          } else {
            var newvalues = { $set: { forgotPasswordToken: token } };

            Protectora.findOneAndUpdate(
              { email: email },
              newvalues,
              {
                //options
                returnNewDocument: true,
                new: true,
                strict: false,
                useFindAndModify: false,
              },
              (err, updated) => {
                console.log(updated);
                res.status(200).json({ message: "Email Sent" });
              }
            );
          }
        });
      }
    });
  }
);

recoveryRouter.get("/tokenValidation-protectora", (req, res) => {
  console.log(req.query.token);
  Protectora.find({ forgotPasswordToken: req.query.token }, (err, data) => {
    if (err || data.length < 1) {
      if (err) console.log(err);
      res.status(200).json({ isValid: false, email: "" });
    } else if (data.length > 0) {
      res.status(200).json({ isValid: true, email: data[0].email });
    }
  });
});

recoveryRouter.get(
  "/resetPassword-protectora",
  cors(corsOptions),
  (req, res) => {
    var newvalues = { $unset: { forgotPasswordToken: req.query.token } };

    Protectora.findOneAndUpdate(
      { email: req.query.email },
      newvalues,
      {
        //options
        returnNewDocument: true,
        new: true,
        strict: false,
        useFindAndModify: false,
      },
      (err, updated) => {
        if (err) console.log(err);
        bcrypt.genSalt(10, (err, salt) => {
          if (err) console.log(err);

          bcrypt.hash(req.query.password, salt, (err, hash) => {
            if (err) console.log(err);
            var newvalues2 = { $set: { password: hash } };
            Protectora.findOneAndUpdate(
              { email: req.query.email },
              newvalues2,
              {
                //options
                returnNewDocument: true,
                new: true,
                strict: false,
                useFindAndModify: false,
              },
              (err, updated) => {
                if (err) console.log(err);

                console.log(updated);
                res.status(200).json({ changed: true });
              }
            );
          });
        });
      }
    );
  }
);

module.exports = recoveryRouter;
